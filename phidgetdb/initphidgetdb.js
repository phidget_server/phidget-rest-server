db.createUser(
    {
        user: "phidget",
        pwd: "phidget1234",
        roles: [
            { role: "dbOwner", db: "phidget" }
        ]
    }
);
db.createCollection("servers");
db.servers.insertOne(
    {
        "address_port":
        {
            "address":"localhost",
            "port":5001
        },
        "server_name": "default", 
        "name":"default_server",
        "password":"",
        "enabled":"yes",
        "auto_connect": true
    })

from phidgetrest.authentication import *
import os

MONGO_HOST = os.environ.get('MONGO_HOST', 'localhost')
MONGO_PORT = int(os.environ.get('MONGO_PORT', 27017))

# Mongo Configuration
MONGO_USERNAME = os.environ.get('MONGO_USERNAME', 'phidget')
MONGO_PASSWORD = os.environ.get('MONGO_PASSWORD', 'phidget1234')

MONGO_DBNAME = os.environ.get('MONGO_DBNAME', 'phidget')

# websocket configuration

WS_PORT = int(os.environ.get('WS_PORT', 5002))

# Enable reads (GET), inserts (POST) and DELETE for resources/collections
# (if you omit this line, the API will default to ['GET'] and provide
# read-only access to the endpoint).
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']

# Enable reads (GET), edits (PATCH), replacements (PUT) and deletes of
# individual items  (defaults to read-only item access).
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

ALLOWED_ROLES = ['user', 'admin', 'superuser']
XML = False


# swagger CORS support
DOM_ENV = os.environ.get('DOM_ENV', 'localhost')
DOM_PORT = os.environ.get('DOM_PORT', '5100')
X_DOMAINS = ['http://{dom}:{dom_port}'.format(dom=DOM_ENV, dom_port=DOM_PORT),'http://elricsubuntuserver:5100', 'http://localhost:5100',  # The domain where Swagger UI is running
             ]
X_HEADERS = ['Content-Type', 'If-Match']  # Needed for the "Try it out" buttons

# scheme validation
schema = {
    # Schema definition, based on Cerberus grammar. Check the Cerberus project
    # (https://github.com/pyeve/cerberus) for details.
    'firstname': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 20,
    },
    'lastname': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 25,
        'required': True,
        # talk about hard constraints! For the purpose of the demo
        # 'lastname' is an API entry-point, so we need it to be unique.
        'unique': True,
    },
    # 'role' is a list, and can only contain values from 'allowed'.
    'role': {
        'type': 'list',
        'allowed': ["author", "contributor", "copy"],
    },
    # An embedded 'strongly-typed' dictionary.
    'location': {
        'type': 'dict',
        'schema': {
            'address': {'type': 'string'},
            'city': {'type': 'string'}
        },
    },
    'born': {
        'type': 'datetime',
    },
}

people = {
    # 'title' tag used in item links. Defaults to the resource title minus
    # the final, plural 's' (works fine in most cases but not for 'people')
    'item_title': 'person',

    # by default the standard item entry point is defined as
    # '/people/<ObjectId>'. We leave it untouched, and we also enable an
    # additional read-only entry point. This way consumers can also perform
    # GET requests at '/people/<lastname>'.
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'lastname'
    },

    # We choose to override global cache-control directives for this resource.
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,

    # most global settings can be overridden at resource level
    'resource_methods': ['GET', 'POST'],

    'schema': schema
}

schema_phidget_servers = {
    'address_port': {
        'type': 'dict',
        'unique': True,
        'required': True,
        'schema': {
            'address': {
                'type': 'string',
                'regex': r'([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})'
                         r'|([0-9a-fA-F]{4}::[0-9a-fA-F]{4}:[0-9a-fA-F]{3}:[0-9a-fA-F]{4}:[0-9a-fA-F]{4})',
            },
            'port': {
                'type': 'integer',
                'min': 1000,
                'max': 65535,
            }
        }
    },
    'server_name': {
        'type': 'string',
        'minlength': 3,
        'maxlength': 20,
        'required': True
    },
    'name': {
        'type': 'string',
        'required': True,
        'unique': True,
    },

    'password': {
        'type': 'string',
        'default': '',
    },
    'enabled': {
        'type': 'string',
        'allowed': ['yes', 'no'],
        'default': 'no'
    },
    'auto_connect': {
        'type': 'boolean',
        'default': True,
    }
}

phidget_server = {
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'name'
    },
    'authentication': RolesAuth,
    'schema': schema_phidget_servers
}

schema_accounts = {
    'username': {
        'type': 'string',
        'required': True,
        'unique': True,
    },
    'password': {
        'type': 'string',
        'required': True,
    },
    'roles': {
        'type': 'list',
        'allowed': ['user', 'superuser', 'admin'],
        'required': True,
    },
    'token': {
        'type': 'string',
        'required': True,
    }
}

accounts = {
    # the standard account entry point is defined as
    # '/accounts/<ObjectId>'. We define  an additional read-only entry
    # point accessible at '/accounts/<username>'.
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'username',
    },
    'authentication': RolesAuth,

    # We also disable endpoint caching as we don't want client apps to
    # cache account data.
    'cache_control': '',
    'cache_expires': 0,

    # Only allow superusers and admins.
    'allowed_roles': ['superuser', 'admin'],
    'allowed_item_roles': ['superuser', 'admin'],

    # Finally, let's add the schema definition for this endpoint.
    'schema': schema_accounts,
}

controller_config = {
    'log_level': 'DEBUG',
    'log_disabled': False,
    'server_log_level': 'DEBUG',
    'server_log_disabled': False,
    'device_log_level': 'DEBUG',
    'device_log_disabled': False
}

device_group_schema = {
    'name': {'type': 'string', 'unique': True},
    'configuration': {'type': 'list',
                      'schema': {'type': 'string'}}
}

device_group = {
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'name',
    },
    'authentication': RolesAuth,
    'schema': device_group_schema
}

device_configuration_schema = {
    'name': {'type': 'string', 'unique': True, 'required': True},
    'configuration': {'required': True,
                      'type': 'dict',
                      'keyschema': {'type': "string"},
                      'valueschema': {'type': 'dict',
                                      'keyschema': {'type': 'string'},
                                      'valueschema': {'type': 'list',
                                                      'schema': {'type': 'integer'}}}
                      }
}

device_configuration = {
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'name',
    },
    'authentication': RolesAuth,
    'schema': device_configuration_schema
}

devices_schema = {
    'serial': {'type': 'integer', 'unique': True, 'readonly': True},
    'name': {'type': 'string', 'unique': True},
    'outputs': {'type': 'integer', 'readonly': True},
    'connected': {'type': 'boolean', 'readonly': True}
}
devices = {
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'name',
    },
    'authentication': RolesAuth,
    'resource_methods': ['GET'],
    'item_methods': ['GET', 'PATCH'],
    'schema': devices_schema
}

settings_schema = {
    'name': {'type': 'string', 'unique': True, 'required': True},
    'settings': {'type': 'dict',
                 'keyschema': {'type': 'string'}
                 }
}

settings = {
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'name',
    },
    'authentication': RolesAuth,
    'resource_methods': ['GET', 'POST'],
    'item_methods': ['GET', 'PATCH'],
    'schema': settings_schema

}

DOMAIN = {
    'servers': phidget_server,
    'accounts': accounts,
    'basic_auth': {'authentication': AccountBasicAuth, 'resource_methods': []},
    'roles_auth': {'authentication': RolesAuth, 'resource_methods': []},
    'devicegroups': device_group,
    'deviceconfigurations': device_configuration,
    'devices': devices,
    'settings': settings,
}

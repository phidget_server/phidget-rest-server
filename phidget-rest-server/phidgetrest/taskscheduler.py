import asyncio
import uuid
import time
import baselogger
from sortedcontainers import SortedDict
from datetime import datetime
import timeit

# some helpers for dev
from phidgetrest import helpers


class SchedulerDB(object):
    def __init__(self, data=None):
        if data is None:
            self.db = {}
        else:
            self.db = data

    def update_resource(self, resource, data: dict):
        self.db.get(resource).update(data)

    def resource_exists(self, resource):
        return resource in self.db

    def add_resource(self, resource: str):
        if self.resource_exists(resource):
            return False
        else:
            self.db.update({resource: SortedDict()})
            return True

    def get_previous_time(self, resource, sched_time):
        # will be next closest object to the right or same if times match
        schedule: SortedDict = self.db.get(resource)
        if not schedule:
            return None

        first_index = schedule.bisect_left(sched_time)

        try:
            section_time = schedule.iloc[first_index]
        except IndexError:
            # if index is zero then
            section_time = schedule.iloc[-1]

        # got closest, now get previous if available
        if section_time != sched_time and first_index != 0:
            section_time = schedule.iloc[first_index - 1]

        return section_time

    def next_tasks(self, level=0, filter_key=None) -> (str, dict):
        if filter_key is None:
            keys = self.db.keys()
        else:
            keys = filter(lambda x: filter_key in x, self.db.keys())

        for res in keys:
            try:
                start_time: dict = self.db.get(res).iloc[level]
            except IndexError:
                continue
            data = self.db.get(res).get(start_time)
            yield res, start_time, data

    def get_data(self, resource, scheduled_time, data):
        return self.db.get(resource).get(scheduled_time)[data]

    def remove_resource_time(self, resource, scheduled_time):
        return self.db.get(resource).pop(scheduled_time)

    def remove_id(self, id):
        to_remove = []
        for resource, schedule in self.db.items():
            for t, data in schedule.items():
                if data.get('uuid') == id:
                    to_remove.append((resource, t))
        for res, t in to_remove:
            self.remove_resource_time(res, t)

    def __bool__(self):
        for values in self.db.values():
            if values:
                return True
        return False


class Scheduler(object):
    def __init__(self, loop=None, callback=None, autostart=True, loglevel='DEBUG', log_disabled=False):
        self._logger = baselogger.Logger('Scheduler', loglevel=loglevel, filename='scheduler.log', file_date=False)
        self.loop = asyncio.get_event_loop() if loop is None else loop
        self.queue = asyncio.Queue()
        # self.db = {}
        self.db = SchedulerDB()
        self._running = False
        self._callback = callback
        self.run_task = None
        self._scheduled_tasks = {}
        self._is_scheduled = False

        self._logger.disabled = log_disabled
        # start processing tasks in queue, option to set autostart mainly for testing but can start in main if required
        if autostart:
            self.start()

    async def stop(self):
        self._logger.file_console.debug('Stopping Scheduler')
        self._running = False
        await self.queue.put((None, 'stop'))

    def start(self):
        self.run_task = asyncio.ensure_future(self._run(), loop=self.loop)
        return self.run_task

    async def _run(self):
        """
        Processes tasks added to que and handles adding and cancelling callbacks
        :return: None
        """
        self._logger.file_console.debug('Running Scheduler')
        self._running = True
        while True:
            task, res = await self.queue.get()  # type: (dict, str)
            if res == 'stop':
                self._logger.file_console.info('Received Stop Command')
                break

            self.db.update_resource(res, task)

            # update scheduled tasks
            await self.update_schedule()

    async def update_schedule(self, filter_key=None):
        """
        upadate the schedule
        :param filter_key: specify a filter for tasks to update
        :type filter_key: bool
        :return:
        """
        # schedule next tasks
        for res, start_time, data in self.db.next_tasks(filter_key=filter_key):
            if data.get('uuid') not in self._scheduled_tasks:
                data.update({'scheduled': True})
                task_id = data.get('uuid')
                # schedule call back
                future = asyncio.ensure_future(self.run_call(start_time, data), loop=self.loop)
                self._scheduled_tasks[task_id] = future
            else:
                data.update({'scheduled': True})

        # check if any pending tasks have another tasks in front
        for res, start_time, data in self.db.next_tasks(level=1, filter_key=filter_key):
            if data.get('uuid') in self._scheduled_tasks:
                data.update({'scheduled': False})
                # cancel task
                future = self._scheduled_tasks.pop(data.get('uuid'))
                future.cancel()

    def cancel_task(self, task_id):
        # cancel future
        try:
            future = self._scheduled_tasks.pop(task_id)
        except KeyError:
            return {'error': 'No active task id {}'.format(task_id)}

        future.cancel()
        # remove task from db
        self.db.remove_id(task_id)
        return {'Status': {task_id: "Cancelled"}}

    def get_running_tasks(self, coros=True):
        """
        Returns a list of co_routines for currently scheduled tasks
        :return:
        """
        if coros:
            return list(self._scheduled_tasks.values())
        else:
            task_list = list(self._scheduled_tasks.keys())
            return {'tasks': task_list}

    async def run_call(self, at_time, data):
        current_time = time.time()
        if float(at_time) < current_time:
            delay = 0
        else:
            delay = at_time - current_time
        await asyncio.sleep(delay)
        result = await self._callback(data)
        # remove from list and db
        self.db.remove_id(data.get('uuid'))
        self._scheduled_tasks.pop(data.get('uuid'))
        # update schedule after each call
        await self.update_schedule(filter_key=data.get('resource'))

    async def add_resources(self, resources: [str]) -> dict:
        """
        Adds resources for scheduling
        :param resources: List of resources
        :return: dict with errors, skipped and added keys
        """
        self._logger.file_console.info('Adding Resources {0}'.format(resources))
        skipped_keys = []
        added_keys = []
        return_dict = {'error': None,
                       'added': added_keys,
                       'skipped': skipped_keys}
        for res in resources:
            if self.db.add_resource(res):
                added_keys.append(res)
            else:
                self._logger.file_console.warning('Resource {0} Already In DB, Skipping'.format(res))
                skipped_keys.append(res)
            await asyncio.sleep(0)

        if skipped_keys:
            return_dict['error'] = 'duplicate_keys'

        return return_dict

    async def add_task(self, task: list, auto_add_resources=False):
        """
        Adds a task to the db, returns success if no conflicts or conflicting tasks and resources if so.
        :param auto_add_resources: Flag to Automatically add missing resources
        :type auto_add_resources: bool
        :param task: dictionary containing task data in format {'start': unix time stamp, 'duration': seconds,
        'resources': list of resource names}
        :type task: dict
        :return: dictionary data containing uuid start time and end time, blocking task and resources or error dict
        :rtype: typing.Coroutine[list[dict]]
        """

        # check for resources
        resources = []
        for d in task:
            for k in d.get('resources'):
                resources.append(k)
        undefined_resources = []
        for res in resources:
            if not self.db.resource_exists(res):
                if auto_add_resources:
                    await self.add_resources([res])
                else:
                    undefined_resources.append(res)
        if undefined_resources:
            return {'error': 'undefined_resources',
                    'resources': undefined_resources}

        # confirm tasks don't conflict with themselves, prevents issues with trying to batch add tasks
        db = SchedulerDB()
        for t in task:
            data = t.get('data')
            for res in t.get('resources'):
                db.add_resource(res)
                start_time = data.get('start')
                duration = data.get('duration')
                result = await self.check_resource(res, start_time, duration, db=db)
                if result is None:
                    db.update_resource(res, {start_time: data})
                else:
                    return {'error': 'conflict', 'blockedby': result, 'blocked': {res: data}}

        # confirm resource is free
        resource_allocation = {}
        for t in task:
            data = t.get('data')
            for res in t.get('resources'):
                start_time = data.get('start')
                duration = data.get('duration')
                result = await self.check_resource(res, start_time, duration)
                if result is not None:
                    resource_allocation.update(result)

        if resource_allocation:
            resource_allocation.update({'error': 'busy'})
            return resource_allocation

        # Tasks not blocked, create uuids and add to queues for processing
        new_tasks = {}
        task_summary = {}
        for t in task:
            id = uuid.uuid4().hex
            data = t.get('data')
            for res in t.get('resources'):
                start_time = data.get('start')
                duration = data.get('duration')
                new_task = {start_time: {'uuid': id,
                                         'duration': duration,
                                         'data': data.get('data'),
                                         'start': start_time,
                                         'resource': res}}
                # add task to queue for processing
                new_tasks.update({res: new_task})
                await self.queue.put((new_task, res))

            task_summary.update({id: {'resources': t.get('resources'),
                                      'duration': data.get('duration'),
                                      'start_time': datetime.utcfromtimestamp(float(data.get('start'))),
                                      'action': data.get('data').get('action')}
                                 })
        await asyncio.sleep(0)
        return new_tasks, task_summary

    async def check_resource(self, resource, start_time, duration, db=None):
        if db is None:
            db = self.db
        first_section_time = db.get_previous_time(resource, start_time)
        # if None then resource is empty
        if first_section_time is None:
            return None

        second_section_time = db.get_previous_time(resource, start_time + duration)

        # if first time and second time do not match then crosses over times
        if first_section_time == second_section_time and first_section_time != start_time:
            # use line math to find if times overlap
            first_section_end = db.get_data(resource, first_section_time, 'duration') + first_section_time
            if not (first_section_end - start_time > 0 and ((start_time + duration) - first_section_time) > 0):
                return None

        # try to get uuid as task may not have one during mock db checking
        try:
            id = db.get_data(resource, first_section_time, 'uuid')
        except KeyError:
            id = 'Not Allocated'
        return {resource: {'state': 'busy',
                           'uuid': id,
                           'start': first_section_time,
                           'duration': db.get_data(resource, first_section_time, 'duration')}}

    def __bool__(self):
        if self.db:
            return True
        return False


if __name__ == '__main__':
    pass

from phidgetrest.tests.fixtures import *
from phidgetrest import validation_schemas
from cerberus import SchemaError, DocumentError

set_command = {
    'action': 'set_closed',
    'ports': [0, 1],
    'device': '1234567',
}

cycle_command = {
    'action': 'cycle',
    'ports': [0, 2],
    'state_end': 'set_open',
    'cycles': 3,
    'time_open': 100,
    'time_closed': 100,
    'device': '1234567',
    'start_time': 1491886830.0010648}

base_command = {
    'action': 'device_command',
    'auto_add_resource': True,
    'commands': [],
}


@pytest.mark.parametrize('times', [
    (1),  # test integer
    (1.1),  # test float
    ([1, 2, 3]),  # test list of integers
    ([1.1, 1.2, 1.3]),  # test list of floats
    ([1, 2.2, 3]),  # test mixed floats and ints
])
def test_validator_cycles(times):
    cycle_command['time_open'] = times
    cycle_command['time_closed'] = times
    base_command['commands'] = [cycle_command]
    result, data = validation_schemas.validate_command(base_command)
    assert result is False


def test_validator_set():
    base_command['commands'] = [set_command]
    result, data = validation_schemas.validate_command(base_command)
    assert result is False


def test_validator_command_set_empty():
    base_command['commands'] = []
    result, data = validation_schemas.validate_command(base_command)
    assert result.get('_status') == 'ERR'


def test_validator_command_set_bad_action():
    base_command['commands'] = [{'action': 'bad_action', 'wrong_field': True}]
    result, data = validation_schemas.validate_command(base_command)
    assert result


def test_validator_command_set_bad_command():
    base_command['commands'] = [{'action': 'set_closed', 'wrong_field': True}]
    result, data = validation_schemas.validate_command(base_command)
    assert result


def test_validator_schema_error():
    result, data = validation_schemas._validate({}, 1)
    assert result.get('_status') == 'ERR'
    assert result.get('_error').get('message') == 'Schema Error'


def test_validator_document_error():
    result, data = validation_schemas._validate(1, {})
    assert result.get('_status') == 'ERR'
    assert result.get('_error').get('message') == 'Document Error'

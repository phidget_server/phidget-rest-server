import pytest
import pytest_mock
import asyncio
import uuid
# from phidgetrest.phidgetcontroller import Controller, Server, Device, run
from phidgetrest import Controller, Server, Device
from phidgetrest.phidgetcontroller import run
# from phidgetrest.taskscheduler import Scheduler, SchedulerDB
from phidgetrest import Scheduler, SchedulerDB
from sortedcontainers import SortedDict
import mock
import time
import sys
import run_config

@pytest.fixture
def mocked_uuid(mocker):
    # simple class to hold some config data
    class MockUUID(object):
        def __init__(self, uuid_hex):
            self.uuid_hex = uuid_hex
            self.mock_uuid = mocker.patch.object(uuid, 'uuid4', autospec=True)
            self.mock_uuid.return_value = uuid.UUID(hex=uuid_hex)
    
    uuid_hex = '5ecd5827b6ef4067b5ac3ceac07dde9f'
    mock_uuid = MockUUID(uuid_hex)
    return mock_uuid


@pytest.fixture
def test_event_loop(event_loop):
    """Create an instance of the default event loop for each test case."""
    event_loop.set_debug(True)
    yield event_loop
    

@pytest.fixture
def mock_caller(mocker):
    return mocker.Mock()

@pytest.fixture
def mock_async_caller():
    return AsyncMock()

@pytest.fixture
def controller(test_event_loop, mock_interface_kit, mock_phidget_manager):
    server_config = {
        'ip_address': '10.10.10.10',
        'server_name': 'test_server',
        'name': 'test_server',
        'port': 5001,
        'password': '',
        'enabled': True,
        'auto_connect': False,
    }
    device_config = {
        'address': '10.10.10.10',
        'serial': 1234567,
        'name': 'test_device_name',
        'type': 'PhidgetInterfaceKit'
    }
    controller_config = {
        'log_level': 'DEBUG',
        'log_disabled': False,
        'server_log_level': 'DEBUG',
        'server_log_disabled': True,
        'device_log_level': 'DEBUG',
        'device_log_disabled': False
    }
    c = Controller(**controller_config, loop=test_event_loop)
    s = Server(server_config, loop=test_event_loop)
    c._servers['test_server'] = {'device': s, 'connected': False}
    device = Device(device_config, digital_outputs=8, loop=test_event_loop)
    device._ready = True
    a_device = {1234567: {'device': device}}
    c._devices.update(a_device)
    s._available_devices.update(a_device)
    return c
    


@pytest.fixture
def mock_server(mocker):
    mocked = mocker.patch('phidgetrest.phidgetcontroller.Server', auto_spec=True, create=True)
    # mocked.get_error = mocker.Mock()
    mocked.get_error.return_value = {}
    mocked.return_value = mocked
    return mocked

@pytest.fixture
def server(test_event_loop, mock_phidget_manager):
    server_config = {
        'ip_address': '10.10.10.10',
        'server_name': 'test_server',
        'name': 'test_server',
        'port': 5001,
        'password': '',
        'enabled': True,
        'auto_connect': False,
    }
    servr = Server(server_config, loop=test_event_loop)
    return servr


@pytest.fixture
def device(test_event_loop, mock_interface_kit):
    device_config = {
        'address': '10.10.10.10',
        'type': 'PhidgetInterfaceKit',
        'name': 'test_device',
        'port': 5001,
        'password': '',
        'serial': 1234567,
        'auto_connect': False,
    }
    dev = Device(device_config, loop=test_event_loop, digital_outputs=4, log_disabled=False, log_level='DEBUG')
    dev._ready = True
    return dev

@pytest.fixture
def fake_call(mocker):
    cb = mocker.Mock()
    cb.side_effect = lambda *args, **kwargs: args[0]()
    return cb


@pytest.fixture
def mock_phidget_manager(mocker, fake_call):
    mocked = mocker.patch('Phidgets.Manager.Manager', auto_spec=True, spec=True, create=True)
    mocked.isAttachedToServer.return_value = True
    return mocked


@pytest.fixture
def mock_device(mocker):
    return mocker.patch('phidgetrest.phidgetcontroller.Device', auto_spec=True, spec=True, create=True)


@pytest.fixture
def mock_interface_kit(mocker):
    mocked = mocker.patch('Phidgets.Devices.InterfaceKit.InterfaceKit', auto_spec=True, spec=True, create=True)
    mocked.getSerialNum.return_value = 1234567
    return mocked


class AsyncMock(mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)
    
@pytest.fixture
def scdlr(test_event_loop, mocker: pytest_mock.MockFixture):
    cb = AsyncMock()
    scheduler = Scheduler(loop=test_event_loop, autostart=False, callback=cb)
    return scheduler


@pytest.fixture
def scdlr_auto(test_event_loop, mocker: pytest_mock.MockFixture):
    cb = AsyncMock()
    scheduler = Scheduler(loop=test_event_loop, autostart=True, callback=cb)
    return scheduler


@pytest.fixture(scope='module')
def db():
    # generate a fixed db object for testing
    # base time to base starttime for all resources
    base_time = 1491886830.0010648
    duration = 100.0
    gap = 200.0
    cycle_command = {'action': 'cycle',
                     'cycles': 4,
                     'time_off': 10,
                     'time_on': 15,
                     'state_start': 'set_open',
                     'state_end': 'set_closed'}
    switch_command = {'action': 'set_toggle'}
    rs = ['d{0}:p{1}'.format(x, y) for x in range(2) for y in range(4)]
    # generate 20 sample times and durations with uuid's and commands
    blocks_generated = {base_time + (gap * x): {'uuid': uuid.uuid4().hex,
                                                'duration': duration,
                                                'data': cycle_command,
                                                'start': base_time + (gap * x),
                                                'resources': rs}
                        for x in range(20)}
    # generate resource dict with sorteddicts as values
    res = {x: SortedDict(blocks_generated) for x in rs}
    # res = {'d{0}:p{1}'.format(x, y): SortedDict(blocks_generated) for x in range(2) for y in range(4)}
    return SchedulerDB(res)


@pytest.fixture(scope='module')
def resources():
    return ['d{0}:p{1}'.format(x, y) for x in range(2) for y in range(4)]


@pytest.fixture()
def mocked_time(mocker):
    # simple class to hold some config data
    class MockTime(object):
        def __init__(self, fixed_time):
            self.time = fixed_time
            self.mock_time = mocker.patch.object(time, 'time', autospec=True)
            self.mock_time.return_value = fixed_time
    
    fixed_time = 1491886830.00000
    mock_time = MockTime(fixed_time)
    return mock_time

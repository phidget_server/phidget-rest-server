# -*- coding: utf-8 -*-

import unittest
from phidgetrest import eventer
from phidgetrest.eventer import Events, Event
import mock
import pytest


class TestBase(unittest.TestCase):
    def setUp(self):
        self.events = Events()

    def callback1(self):
        pass

    def callback2(self):
        pass

    def callback3(self):
        pass


class TestEvents(TestBase):
    def test_getattr(self):
        class MyEvents(Events):
            __events__ = ('event_One')

        try:
            MyEvents()._event_NotOne += self.callback1
        except AttributeError as e:
            pass
        else:
            self.fail("'EventsException' expected and not raised.")

        try:
            self.events._event_One += self.callback1
        except Exception as e:
            self.fail("Exception raised but not expected.")

    def test_len(self):
        self.events.on_change += self.callback1
        self.events.on_get += self.callback2
        self.assertEqual(len(self.events), 2)

    def test_iter(self):
        self.events.on_change += self.callback1
        self.events.on_change += self.callback2
        self.events.on_edit += self.callback1
        i = 0
        for event in self.events:
            i += 1
            self.assertTrue(isinstance(event, eventer._EventSlot))
        self.assertEqual(i, 2)
        
    def test_repr(self):
        result = str(self.events)
        assert 'eventer.Events object at' in result


class TestEventSlot(TestBase):
    def setUp(self):
        super(TestEventSlot, self).setUp()
        self.events.on_change += self.callback1
        self.events.on_change += self.callback2
        self.events.on_change += self.callback3
        self.events.on_edit += self.callback3

    def test_type(self):
        ev = self.events.on_change
        self.assertTrue(isinstance(ev, eventer._EventSlot))
        self.assertEqual(ev.__name__, 'on_change')

    def test_len(self):
        self.assertEqual(len(self.events.on_change), 3)
        self.assertEqual(len(self.events.on_edit), 1)

    def test_repr(self):
        ev = self.events.on_change
        self.assertEqual(ev.__repr__(), "event 'on_change'")

    def test_iter(self):
        ev = self.events.on_change
        self.assertEqual(len(ev), 3)
        i = 0
        for target in ev:
            i += 1
            self.assertEqual(target.__name__, 'callback%d' % i)

    def test_getitem(self):
        ev = self.events.on_edit
        self.assertEqual(len(ev), 1)
        self.assertTrue(ev[0].__name__, 'callback3')
        try:
            ev[1]
        except IndexError:
            pass
        else:
            self.fail("IndexError expected.")

    def test_isub(self):
        self.events.on_change -= self.callback1
        ev = self.events.on_change
        self.assertEqual(len(ev), 2)
        self.assertEqual(ev[0].__name__, 'callback2')
        self.assertEqual(ev[1].__name__, 'callback3')
    
    def test_call(self):
        cb = mock.Mock()
        cb1 = mock.Mock()
        self.events.on_change += cb
        self.events.on_change += cb1
        self.events.on_change()
        cb.assert_called_once()
        cb1.assert_called_once()
        
    def test_repr(self):
        result = str(self.events.on_change)
        assert "event 'on_change'" == result

def test_Event():
    event_kwargs = {'name': 'test', 'extended': {'one': 1, 'two': 2}}
    event_args = (Events(), Events())
    event_event = 'test'
    event_data = 'test_data'
    an_event = Event(*event_args, **event_kwargs, event=event_event, data=event_data)
    assert an_event.data == event_data
    assert an_event.event == event_event
    # assert len(an_event.objs) == len(event_args)
    # for obj in an_event.objs:
    #     assert isinstance(obj, Events)
    assert getattr(an_event, 'name') == 'test'
    assert getattr(an_event, 'extended') == event_kwargs.get('extended')
    # assert getattr(an_event.extended, 'two') == 2
    
# class TestInstanceEvents(TestBase):
#
#     def test_getattr(self):
#
#         MyEvents = Events(('eventOne'))
#
#         try:
#             MyEvents.eventOne += self.callback1
#         except:
#             self.fail("Exception raised but not expected.")
#
#         try:
#             MyEvents.on_eventNotOne += self.callback1
#         except Exception:
#             pass
#         else:
#             self.fail("'EventsException' excpected and not raised.")
#
#         try:
#             self.events.on_eventNotOne += self.callback1
#         except:
#             self.fail("Exception raised but not expected.")
#
#     def test_instance_restriction(self):
#
#         class MyEvents(Events):
#             __events__ = ('on_eventOne', 'on_eventTwo')
#
#         MyRestrictedInstance = MyEvents(('on_everyTwo', ))
#
#         try:
#             MyRestrictedInstance.on_everyTwo += self.callback1
#         except:
#             self.fail("Exception raised but not expected.")
#
#         try:
#             MyRestrictedInstance.on_everyOne += self.callback1
#         except:
#             pass
#         else:
#             self.fail("'EventsException' excpected and not raised.")

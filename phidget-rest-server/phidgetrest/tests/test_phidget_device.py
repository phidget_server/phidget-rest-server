from phidgetrest.tests.fixtures import *
from phidgetrest.eventer import Event
# from phidgetrest.phidgetcontroller import ServerEvent, DeviceEvent, ControllerEvent
from phidgetrest import ServerEvent, DeviceEvent, ControllerEvent
from Phidgets.Events.Events import OutputChangeEventArgs, AttachEventArgs, DetachEventArgs, ErrorEventArgs
from Phidgets.PhidgetException import PhidgetException
from phidgetrest.helpers import pretty_print


def test_open_close(device):
    device.open()
    device.interfacekit.openRemoteIP.assert_called_once()
    device.close()
    device.interfacekit.closePhidget.assert_called_once()


@pytest.mark.parametrize('data', [
    ({'action': 'set_closed', 'ports': [0, 1]}),
    ({'action': 'set_open', 'ports': [0, 1]}),
    ({'action': 'set_toggle', 'ports': [0, 1]}),
])
@pytest.mark.asyncio
async def test_run_commands(device, data, mocked_uuid, mock_caller):
    device.event_on_any += mock_caller
    command = {'id': mocked_uuid.uuid_hex,
               'data': data}
    await device.run_command(command)
    while mock_caller.call_count < 2:
        await asyncio.sleep(0)
    assert device.interfacekit.setOutputState.call_count == len(data.get('ports'))


@pytest.mark.asyncio
async def test_run_command_no_action(device):
    command = {'data': {'no_action': 'bad_action',
                        'ports': [1, 3]}}
    result = await device.run_command(command)
    assert result is False

@pytest.mark.asyncio
async def test_run_command_device_not_ready(device):
    command = {'data': {'no_action': 'bad_action',
                        'ports': [1, 3]}}
    device._ready=False
    with pytest.raises(IOError):
        result = await device.run_command(command)

@pytest.mark.asyncio
async def test_run_command_device_starts_not_ready(device, mock_caller):
    device.event_on_any += mock_caller
    async def set_device_ready():
        await asyncio.sleep(0.2)
        device._ready = True
        
    command = {'data': {'action': 'set_closed',
                        'ports': [1, 3]}}
    device._ready = False
    asyncio.ensure_future(set_device_ready())
    result = await device.run_command(command)
    while mock_caller.call_count < 2:
        await asyncio.sleep(0)
    assert device.interfacekit.setOutputState.call_count == len(command.get('data').get('ports'))
    
@pytest.mark.asyncio
async def test_run_command_bad_action(device):
    command = {'data': {'action': 'bad_action',
                        'ports': [1, 3]}}
    result = await device.run_command(command)
    assert result is False

@pytest.mark.parametrize('time_on,time_off', [
    (0,0),
    ([0,0,0],[0,0,0])
])
@pytest.mark.asyncio
async def test_run_command_cycles(device, time_on, time_off):
    data = {'action': 'cycle',
            'ports': [1, 0, 3],
            'cycles': 3,
            'time_open': time_on,
            'time_closed': time_off}
    command = {'data': data}
    result = await device.run_command(command)
    await asyncio.sleep(0)
    expected_call_count = (2 + (data.get('cycles') * 2)) * len(data.get('ports'))
    assert device.interfacekit.setOutputState.call_count == expected_call_count

@pytest.mark.slow
@pytest.mark.parametrize('time_on,time_off', [
    (0.1,0.1)
])
@pytest.mark.asyncio
async def test_run_command_cycles_slow(device, time_on, time_off):
    data = {'action': 'cycle',
            'ports': [1, 0, 3],
            'cycles': 20,
            'time_open': time_on,
            'time_closed': time_off}
    command = {'data': data}
    result = await device.run_command(command)
    expected_call_count = (2 + (data.get('cycles') * 2)) * len(data.get('ports'))
    assert device.interfacekit.setOutputState.call_count == expected_call_count
    await asyncio.sleep(0)

@pytest.mark.parametrize('action, ports', [
    ('set_closed', [5]),
    ('set_open', [-1]),
])
@pytest.mark.asyncio
async def test_change_port_port_count_wrong(device, action, ports):
    with pytest.raises(PhidgetException):
        future = asyncio.ensure_future(device._change_port(ports, action))
        while not future.done():
            await asyncio.sleep(0)
        future.result()


@pytest.mark.asyncio
async def test_change_port_bad_action(device):
    action = 'bad_action'
    ports = [0, 3]
    result = await device._change_port(ports, action)
    assert result is False

@pytest.mark.asyncio
async def test_event_output_change(device, mock_caller, mock_interface_kit):
    event_name = 'event_on_output_change'
    setattr(device, event_name, mock_caller)
    futures = device._event(OutputChangeEventArgs(index=1, state=True, device=mock_interface_kit))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()

@pytest.mark.asyncio
async def test_event_device_attach(device, mock_caller, mock_interface_kit):
    event_name = 'event_on_device_attach'
    setattr(device, event_name, mock_caller)
    futures = device._event(AttachEventArgs(device=mock_interface_kit))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()

@pytest.mark.asyncio
async def test_event_device_detach(device, mock_caller, mock_interface_kit):
    event_name = 'event_on_device_detach'
    setattr(device, event_name, mock_caller)
    futures = device._event(DetachEventArgs(device=mock_interface_kit))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()

@pytest.mark.asyncio
async def test_event_error(device, mock_caller, mock_interface_kit):
    event_name = 'event_on_error'
    setattr(device, event_name, mock_caller)
    futures = device._event(ErrorEventArgs(eCode=99, description='Test_error', device=mock_interface_kit))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()


def test_event_unknown_device_error(device, mock_caller):
    event_name = 'event_on_error'
    setattr(device, event_name, mock_caller)
    result = device._event(DeviceEvent(type=-1, name='test error', data=None))
    assert result is None


def test_event_unknown(device, mock_caller):
    event_name = 'event_on_error'
    setattr(device, event_name, mock_caller)
    result = device._event(ServerEvent(type=-1, name='unhandled event', data=None))
    assert result is None

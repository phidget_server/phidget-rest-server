import pytest
import pytest_mock
import time

from mock import call

# from phidgetrest.taskscheduler import Scheduler, SchedulerDB
from phidgetrest import Scheduler, SchedulerDB
import uuid
from sortedcontainers import SortedDict
import asyncio
# import some helpers
from phidgetrest import helpers
from phidgetrest.tests.fixtures import *


# @pytest.mark.skip
# def test_mocks(scdlr: Scheduler, mocker, resources):
#     # This is how to mock to test function independently, an example of a mocks i might use
#     # this is here to test my mocks
#     scdlr.db = {'d{0}:p{1}'.format(x, y): None for x in range(2) for y in range(4)}
#     mocked_scdlr = mocker.patch.object(scdlr, 'add_resources', autospec=True)
#
#     expected = {'error': None, 'added': resources, 'skipped': []}
#     not_expected = {'error': 1, 'added': resources, 'skipped': ['p1']}
#     mocked_scdlr.return_value = expected
#
#     result = scdlr.add_resources(['test'])
#     assert result == expected
#     # just to confirm it works
#     assert result != not_expected
#
#     mock_uuid = mocker.patch.object(uuid, 'uuid4', autospec=True)
#     mock_uuid.return_value = uuid.UUID(hex='5ecd5827b6ef4067b5ac3ceac07dde9f')
#     # this would return a different value if this wasn't the case
#     assert uuid.uuid4().hex == '5ecd5827b6ef4067b5ac3ceac07dde9f'


# @pytest.mark.skip
# def test_mockers(mocked_uuid):
#     # this would return a different value if this wasn't the case
#     assert uuid.uuid4().hex == '5ecd5827b6ef4067b5ac3ceac07dde9f'


@pytest.mark.asyncio
async def test_add_task_resources_exists(scdlr: Scheduler, db: dict, mocked_uuid, resources):
    """
    tests adding a task, this should be successfully and task added to db
    :param scdlr:
    :param db:
    :return:
    """
    # Init db to test object
    scdlr.db = db
    
    # make sure time is outside prepared db
    start_time = 1591886830.0010648
    duration = 1800
    new_task = {'resources': [resources[0], resources[5]],
                'data': {'start': start_time,
                         'duration': duration,
                         'data': {'afakecommand': 'do_x'}},
                }
    
    added_tasks: dict = await scdlr.add_task([new_task])
    for resource, data in added_tasks.items():
        assert data.get(start_time).get('uuid') == mocked_uuid.uuid_hex
        assert data.get(start_time).get('start') == start_time
        assert data.get(start_time).get('duration') == duration


@pytest.mark.asyncio
async def test_add_task_resources_auto_add(scdlr: Scheduler, db: dict, mocked_uuid):
    """
    tests adding a task, this should be successfully and task added to db
    :param scdlr:
    :param db:
    """
    # Init db to test object
    scdlr.db = db
    # make sure time is outside prepared db
    start_time = 1591886830.0010648
    duration = 1800
    new_task = {'resources': ['d10:p10', 'd11:p11'],
                'data': {'start': start_time,
                         'duration': duration,
                         'afakecommand': 'do_x'},
                }
    
    uuidhex = '5ecd5827b6ef4067b5ac3ceac07dde9f'
    
    added_tasks: dict = await scdlr.add_task([new_task], auto_add_resources=True)
    for resource, data in added_tasks.items():
        assert data.get(start_time).get('uuid') == uuidhex  # get first value as only one key returned
        assert data.get(start_time).get('start') == start_time
        assert data.get(start_time).get('duration') == duration


@pytest.mark.asyncio
async def test_add_task_resource_busy(scdlr: Scheduler, db: dict, resources):
    # Init db to test object
    scdlr.db = db
    
    start_time = 1491886830.0010648
    duration = 200
    new_task = {'resources': [resources[1], resources[5]],
                'data': {'start': start_time,
                         'duration': duration,
                         'resources': resources},
                }
    
    added_task: dict = await scdlr.add_task([new_task])
    busy_res1: dict = added_task.get(resources[1])
    busy_res2: dict = added_task.get(resources[5])
    assert added_task.get('error') == 'busy'
    assert busy_res1.get('state') == 'busy'
    assert busy_res2.get('state') == 'busy'
    assert busy_res1.get('start') == start_time


@pytest.mark.asyncio
async def test_add_task_resources_nonexistent(scdlr: Scheduler, resources):
    start_time = time.time()
    duration = 400
    new_task = {'resources': [resources[0], resources[5]],
                'data': {'start': start_time,
                         'duration': duration,
                         'afakecommand': 'do_x'},
                }
    
    added_task: dict = await scdlr.add_task([new_task])
    # helpers.pretty_print(added_task)
    assert added_task.get('error') == 'undefined_resources'
    assert added_task.get('resources') == new_task.get('resources')


@pytest.mark.asyncio
async def test_add_resources_no_conflict(scdlr: Scheduler, resources):
    expected = {'error': None, 'added': resources, 'skipped': []}
    result = await scdlr.add_resources(resources)
    assert result == expected


@pytest.mark.asyncio
async def test_add_resources_with_conflict(scdlr: Scheduler, resources, db):
    scdlr.db = db
    expected = {'error': 'duplicate_keys', 'added': [], 'skipped': resources}
    result = await scdlr.add_resources(resources)
    assert result == expected


@pytest.mark.parametrize("base_time,duration", [
    (1491886730.0010648, 100),  # insert before any tasks
    (1491886931.0010648, 90),  # after first before second
    (1591886730.0010648, 100),  # after all tasks
])
@pytest.mark.asyncio
async def test_check_resource_is_free(scdlr: Scheduler, resources: list, db, base_time, duration):
    scdlr.db = db
    result = await scdlr.check_resource(resources[0], base_time, duration)
    assert result is None


@pytest.mark.parametrize("base_time,duration", [
    (1491886830.0010648, 800),  # overlap sections
    (1491886840.0010648, 90),  # start during and ending during
    (1491886931.0010648, 100),  # starting free end during
    (1491886840.0010648, 150),  # start during end free
    (1491886931.0010648, 250),  # start free, end free, with busy task in between
    (1491886830.0010648, 90),  # start at same time as other task, end during other task
])
@pytest.mark.asyncio
async def test_check_resource_busy(scdlr: Scheduler, resources, db, base_time, duration):
    scdlr.db = db
    resource = resources[0]
    # expected result
    expected = {resources[0]: {'state': 'busy',
                               'duration': 100.0}}
    
    result = await scdlr.check_resource(resource, base_time, duration)
    # only need to confirm it was detected
    details = result.get(resources[0])
    # print(result)
    details.pop('uuid')
    details.pop('start')
    assert result == expected


@pytest.mark.asyncio
async def test_run(test_event_loop, scdlr, resources):
    """
    confirm
    :param test_event_loop:
    :type test_event_loop: asyncio.SelectorEventLoop
    :param scdlr: Scheduler object
    :type scdlr: Scheduler
    """
    start_time = 1491886830.0010648
    duration = 200
    new_task = {'resources': [resources[0], resources[5]],
                'data': {'start': start_time,
                         'duration': duration,
                         'afakecommand': 'do_x'},
                }
    
    result = scdlr.start()
    rtask = await scdlr.add_task([new_task], auto_add_resources=True)
    await scdlr.stop()
    while not result.done():
        await asyncio.sleep(0)
    assert result.result() is None


@pytest.mark.asyncio
async def test_run_auto_start(scdlr_auto):
    await scdlr_auto.stop()
    while not scdlr_auto.run_task.done():
        await asyncio.sleep(0)
    assert scdlr_auto.run_task.result() is None


@pytest.mark.parametrize("start_time", [
    (1491886820.0010648),  # insert before current_time
    (1491886830.0100000),  # insert_call after current time by 0.01s to make test fast
])
@pytest.mark.asyncio
async def test_schedule_call(test_event_loop, scdlr_auto: Scheduler, resources, mocked_time, start_time):
    """
    confirm
    :param test_event_loop:
    :type test_event_loop: asyncio.SelectorEventLoop
    :param scdlr: Scheduler object
    :type scdlr: Scheduler
    """
    duration = 200
    new_task = {'resources': [resources[0]],
                'data': {'start': start_time,
                         'duration': duration,
                         'afakecommand': 'do_x'},
                }
    rtask = await scdlr_auto.add_task([new_task], auto_add_resources=True)
    await scdlr_auto.stop()
    # wait to finish processing tasks
    while not scdlr_auto.run_task.done():
        await asyncio.sleep(0)
    # wait for call back task to be run
    futures = asyncio.gather(*scdlr_auto.get_running_tasks())
    while not futures.done():
        await asyncio.sleep(0)
    futures.result()
    # helpers.pretty_print(list(rtask[0].values())[0])
    scdlr_auto._callback.assert_called_once_with(rtask.get(resources[0]).get(start_time))


@pytest.mark.asyncio
async def test_reschedule_call(test_event_loop, scdlr_auto: Scheduler, resources, mocked_time):
    """
    confirm two tasks with one inserted after the other is scheduled is correctly handled
    :param test_event_loop:
    :type test_event_loop: asyncio.SelectorEventLoop
    :param scdlr: Scheduler object
    :type scdlr: Scheduler
    """
    duration = 0.001
    later_time = 1491886830.0200000
    new_task_later = {'resources': [resources[0]],
                      'data': {'start': later_time,
                               'duration': duration,
                               'afakecommand': 'do_x'},
                      }
    
    earlier_time = 1491886830.0100000
    new_task_earlier = {'resources': [resources[0]],
                        'data': {'start': earlier_time,
                                 'duration': duration,
                                 'afakecommand': 'do_x'},
                        }
    
    # some other tasks to cover checking of pending tasks not currently scheduled
    other_task_1 = {'resources': [resources[1]],
                    'data': {'start': 1491886730.0100000,
                             'duration': duration,
                             'afakecommand': 'do_x'}}
    other_task_2 = {'resources': [resources[1]],
                    'data': {'start': 1491886830.0200000,
                             'duration': duration,
                             'afakecommand': 'do_x'}}
    
    # add other tasks earliest first
    other_task1 = await scdlr_auto.add_task([other_task_1], auto_add_resources=True)
    other_task2 = await scdlr_auto.add_task([other_task_2], auto_add_resources=True)
    
    # add later task first
    last_task = await scdlr_auto.add_task([new_task_later], auto_add_resources=True)
    
    # add task to be completed sooner
    first_task = await scdlr_auto.add_task([new_task_earlier], auto_add_resources=True)
    
    await scdlr_auto.stop()
    
    # wait to process all tasks
    while scdlr_auto:
        await asyncio.sleep(0)
    
    # check called earlier first
    # helpers.pretty_print([first_task, last_task])
    calls = [call(first_task.get(resources[0]).get(earlier_time)), call(last_task.get(resources[0]).get(later_time))]
    scdlr_auto._callback.assert_has_calls(calls)

# fixtures contains all imports and fixtures for tests
import argparse

from phidgetrest.tests.fixtures import *
# from phidgetrest.eventer import Event
# from phidgetrest.phidgetcontroller import *
from phidgetrest import *
import builtins


@pytest.mark.asyncio
async def test_add_server(controller, mock_phidget_manager):
    server_data = {
        'ip_address': '172.22.2.160',
        'server_name': 'elricsubuntuserver',
        'name': 'main',
        'port': 5001,
        'password': '',
        'enabled': True,
        'auto_connect': False
    }
    
    add_server_message = {
        'action': 'add_server',
        'server_info': server_data
    }
    
    result = await controller.command(add_server_message)
    assert isinstance(result, Server)


@pytest.mark.asyncio
async def test_get_server_errors(controller, mock_phidget_manager):
    server_data = {
        'ip_address': '172.22.2.160',
        'server_name': 'test_name',
        'name': 'test_server',
        'port': 5001,
        'password': '',
        'enabled': True,
        'auto_connect': False
    }
    
    add_server_message = {
        'action': 'add_server',
        'server_info': server_data
    }
    expected = {'test_server': {}}
    server = await controller.command(add_server_message)
    result = controller.get_server_errors()
    assert result == expected
    
    result = controller.get_server_errors('test_server')
    assert result == expected
    
    result = controller.get_server_errors('badname')
    assert result == {'error': 'no server named badname'}
    await controller.stop()

@pytest.mark.asyncio
async def test_add_server_auto_connect(controller, mock_phidget_manager):
    server_data = {
        'ip_address': '172.22.2.160',
        'server_name': 'elricsubuntuserver',
        'name': 'main',
        'port': 5001,
        'password': '',
        'enabled': True,
        'auto_connect': True
    }
    
    add_server_message = {
        'action': 'add_server',
        'server_info': server_data
    }
    
    result = await controller.command(add_server_message)
    
    assert isinstance(result, Server)


@pytest.mark.asyncio
async def test_add_device(controller, mock_interface_kit):
    serial = '290102'
    device_data = {
        'address': '172.22.2.160',
        'serial': serial,
        'name': 'test_name',
        'type': 'PhidgetInterfaceKit'
    }
    
    add_device_message = {
        'action': 'add_device',
        'device_info': device_data
    }
    
    update_device_message = {
        'action': 'add_device',
        'device_info': {'serial': serial, 'name': 'new_name'}
    }
    
    result = await controller.command(add_device_message)
    # while not controller._devices:
    #     await asyncio.sleep(0)
    assert result.get('name') == 'test_name'
    called_with = (device_data.get('address').encode(), 5001, serial, b'')
    result.get('device').interfacekit.openRemoteIP.assert_any_call(*called_with)
    
    # update device info
    result = await controller.command(update_device_message)
    assert result.get('name') == 'new_name'
    assert isinstance(result.get('device'), Device)


@pytest.mark.asyncio
async def test_command_not_supported(controller):
    not_supported_command = {
        'action': 'test_command',
    }
    expected = {'error': 'command not supported'}
    
    result = await controller.command(not_supported_command)
    assert result == expected


@pytest.mark.asyncio
async def test_task_not_supported(controller):
    start_time = 1491886830.0010648
    device_command = {
        'action': 'device_command',
        'auto_add_resource': True,
        'commands': [{
            'action': 'not supported',
            'device': '1234567',
            'ports': [1],
            'start_time': start_time}
        ]}
    expected = {'error': 'task not supported'}
    result = await controller.command(device_command)
    assert result == expected


@pytest.mark.parametrize('timeon,timeoff', [
    (10.0, 5),
    ([10.0, 10.0, 10.0], [5, 5, 5])
])
@pytest.mark.asyncio
async def test_device_command_cycle(controller, mock_interface_kit, mocked_uuid, timeon, timeoff):
    start_time = 1491886830.0010648
    device_command = {
        'action': 'device_command',
        'auto_add_resource': True,
        'commands': [{
            'action': 'cycle',
            'ports': [0, 2],
            'state_start': 'set_closed',
            'state_end': 'set_open',
            'cycles': 3,
            'time_open': timeon,
            'time_closed': timeoff,
            'device': '1234567',
            'start_time': start_time}
        ]}
    tasks = {'1234567:0': {start_time: {'uuid': mocked_uuid.uuid_hex,
                                        'duration': 45.0,
                                        'data': device_command.get('commands')[0],
                                        'resource': '1234567:0',
                                        'start': start_time}},
             '1234567:2': {start_time: {'uuid': mocked_uuid.uuid_hex, 'duration': 45.0,
                                        'data': device_command.get('commands')[0],
                                        'resource': '1234567:2',
                                        'start': start_time}}
             }
    
    result = await controller.command(device_command)
    assert result == tasks


@pytest.mark.asyncio
async def test_device_command_cycle_conflict(controller, mock_interface_kit, mocked_uuid):
    start_time = 1491886830.0010648
    cycles = 3
    time_on = 10.0
    time_off = 5
    duration = (time_on + time_off) * cycles
    device_command = {
        'action': 'device_command',
        'auto_add_resource': True,
        'commands': [{'action': 'cycle',
                      'ports': [0, 2],
                      'state_start': 'set_closed',
                      'state_end': 'set_open',
                      'cycles': cycles,
                      'time_open': time_on,
                      'time_closed': time_off,
                      'device': '1234567',
                      'start_time': start_time},
                     {'action': 'cycle',
                      'ports': [1, 2],
                      'state_start': 'set_closed',
                      'state_end': 'set_open',
                      'cycles': cycles,
                      'time_open': time_on,
                      'time_closed': time_off,
                      'device': '1234567',
                      'start_time': start_time}
                     ]}
    expected = {'error': 'conflict',
                'blockedby': {'1234567:2': {'state': 'busy',
                                            'uuid': 'Not Allocated',
                                            'start': start_time,
                                            'duration': duration}},
                'blocked': {'1234567:2': {'start': start_time,
                                          'duration': duration,
                                          'data': {'action': 'cycle', 'ports': [1, 2], 'state_start': 'set_closed',
                                                   'state_end': 'set_open', 'cycles': cycles, 'time_open': time_on,
                                                   'time_closed': time_off, 'device': '1234567',
                                                   'start_time': start_time}}}}
    result = await controller.command(device_command)
    assert result == expected


@pytest.mark.asyncio
async def test_device_command_set(controller, mocked_uuid):
    start_time = 1491886830.0010648
    device_command = {
        'action': 'device_command',
        'auto_add_resource': True,
        'commands': [
            {
                'action': 'set_closed',
                'ports': [0, 2],
                'device': '1234567',
                'start_time': start_time
            }]}
    tasks = {'1234567:0': {start_time: {'uuid': mocked_uuid.uuid_hex,
                                        'duration': 0.1,
                                        'data': device_command.get('commands')[0],
                                        'resource': '1234567:0',
                                        'start': start_time}},
             '1234567:2': {start_time: {'uuid': mocked_uuid.uuid_hex, 'duration': 0.1,
                                        'data': device_command.get('commands')[0],
                                        'resource': '1234567:2',
                                        'start': start_time}}
             }
    result = await controller.command(device_command)
    assert result == tasks

@pytest.mark.asyncio
async def test_device_command_device_not_available(controller, mocked_uuid):
    start_time = 1491886830.0010648
    dev_serial = '111111'
    device_command = {
        'action': 'device_command',
        'auto_add_resource': True,
        'commands': [
            {
                'action': 'set_closed',
                'ports': [0, 2],
                'device': dev_serial,
                'start_time': start_time
            }]}
    expected = {'error': 'device {} is not available'.format(dev_serial)}
    result = await controller.command(device_command)
    assert result == expected

@pytest.mark.asyncio
async def test_run_device_command(controller, mocked_uuid, mock_caller):
    devices = controller._devices.values()
    device_count = len(devices)
    for data in devices:
        dev = data.get('device')
        dev.event_on_any += mock_caller
    
    start_time = 1491886830.0010648
    device = 1234567
    data = {'action': 'set_closed',
            'ports': [0, 2],
            'device': device,
            'start_time': start_time
            }
    command = {'id': mocked_uuid.uuid_hex,
               'data': data}
    
    result = await controller.run_device_command(command)
    futures = asyncio.gather(*controller.get_scheduled_tasks())
    while not futures.done():
        asyncio.sleep(0)
        
    futures.result()
    while mock_caller.call_count < 2:
        await asyncio.sleep(0)
    mock_ikit = controller._devices.get(device).get('device').interfacekit
    assert mock_ikit.setOutputState.call_count == len(data.get('ports'))


@pytest.mark.asyncio
async def test_run_device_command_device_not_ready(controller, mocked_uuid):
    start_time = 1491886830.0010648
    device = 1234567
    data = {'action': 'set_closed',
            'ports': [0, 2],
            'device': device,
            'start_time': start_time
            }
    command = {'id': mocked_uuid.uuid_hex,
               'data': data}
    d = controller._devices.get(int(device)).get('device')
    d._ready = False
    result = await controller.run_device_command(command)
    assert result is False


@pytest.mark.parametrize("event,data,etype", [
    ('event_on_server_connect', {'name': "test_server"}, ServerEvent.SERVER_CONNECT),
    ('event_on_server_disconnect', {'name': 'test_server'}, ServerEvent.SERVER_DISCONNECT),
    ('event_on_device_attach', {'serial': '1234567'}, ServerEvent.DEVICE_ATTACH),
    ('event_on_device_attach', {'serial': '7654321'}, ServerEvent.DEVICE_ATTACH),
    ('event_on_detach', {'serial': '1234567'}, ServerEvent.DEVICE_DETACH),
    ('event_on_error', {'somedata': 1}, ServerEvent.SERVER_ERROR),
])
@pytest.mark.asyncio
async def test_server_events(controller, mock_caller, event, data, etype):
    setattr(controller, event, mock_caller)
    controller._event(ServerEvent(event=event, data=data, type=etype))
    await asyncio.sleep(0)
    mock_caller.assert_called_once()


@pytest.mark.asyncio
async def test_server_event_on_any(controller, mock_caller):
    event = 'event_on_any'
    data = {'somedata': 1}
    etype = ServerEvent.SERVER_ERROR
    setattr(controller, 'event_on_any', mock_caller)
    controller._event(ServerEvent(event=event, data=data, type=etype))
    await asyncio.sleep(0)
    assert mock_caller.call_count == 2


@pytest.mark.parametrize("event,data,etype", [
    ('event_on_cycle_start', {'somedata': 1}, DeviceEvent.CYCLE_START),
    ('event_on_cycle_end', {'somedata': 1}, DeviceEvent.CYCLE_END),
    ('event_on_error', {'some_data': 1}, DeviceEvent.DEVICE_ERROR),
    ('event_on_output_change', {'some_data': 1}, DeviceEvent.OUTPUT_CHANGE),
    ('event_on_cycle_update', {'some_data': 1}, DeviceEvent.CYCLE_UPDATE),
])
@pytest.mark.asyncio
async def test_device_events(controller, mock_caller, event, data, etype):
    setattr(controller, event, mock_caller)
    controller._event(DeviceEvent(event=event, data=data, type=etype))
    mock_caller.assert_called_once()


@pytest.mark.asyncio
async def test_server_unknown_event(controller):
    result = controller._event(ServerEvent(event='unknown_test', data={'unknown_test'}, type=14))
    assert result is None


@pytest.mark.asyncio
async def test_device_unknown_event(controller):
    result = controller._event(DeviceEvent(event='unknown_test', data={'unknown_test'}, type=14))
    assert result is None


@pytest.mark.asyncio
async def test_unknown_event(controller):
    class badevent():
        def __init__(self, **kwargs):
            for k, v in kwargs.items():
                setattr(self, k, v)
    
    result = controller._event(badevent(event='unknown_test', data={'unknown_test'}, type=14))
    assert result is None


def test_connect_no_args(controller):
    assert controller.connect() is None


def test_connect_args(controller):
    assert controller.connect('test_server') is None


def test_connect_wrong_args(controller):
    assert not controller.connect('bad_server_name')


def test_disconnect_no_args(controller):
    assert controller.disconnect() is None


def test_disconnect_args(controller):
    assert controller.disconnect('test_server') is None


def test_disconnect_wrong_args(controller):
    assert not controller.disconnect('bad_server_name')


def test_connection_status(controller):
    expected = {'test_server': False}
    result = controller.connection_status()
    assert result == expected


@pytest.mark.asyncio
async def test_start_stop(controller):
    controller.start()
    assert not controller.scheduler_task.done()
    await controller.stop()
    await asyncio.sleep(0)
    assert controller.scheduler_task.done()


@pytest.mark.asyncio
async def test_stop_action(controller):
    controller.start()
    
    action_stop = {
        'action': 'stop'
    }
    
    result = await controller.command(action_stop)
    await asyncio.sleep(0)
    assert controller.scheduler_task.done()


def test_get_available_devices_no_args(controller):
    expected = {'test_server': controller._devices}
    result = controller.get_available_devices()
    assert expected == result


def test_get_available_devices_args(controller):
    expected = {'test_server': controller._devices}
    result = controller.get_available_devices('test_server')
    assert expected == result


def test_get_available_devices_wrong_args(controller):
    result = controller.get_available_devices('bad_server_name')
    assert not result


@pytest.mark.slow
@pytest.mark.parametrize('test_args', [
    (["set_closed", "-d", "unit_test_device"]),
    (["set_closed", "-d", "unit_test_device", '-p', '0', '1']),
    (["set_closed", "-d", "unit_test_configuration", '-p', '0', '1', '2']),
    (["set_closed", "-d", "unit_test_configuration"]),
    (["cycle", "-d", "unit_test_configuration", "-c", "3", "-tc", "0.0", "-to", "0.0"]),
    (["cycle", "-d", "unit_test_group:i", "-i", "test_script.txt"]),
    (["cycle", "-d", "unit_test_group:i", "-i", "test_script.txt", "-S", "1"]),
    (["cycle", "-d", "unit_test_configuration", "-c", "2", "-tc", "0.1", "-to", "0.1"]),
])
@pytest.mark.asyncio
async def test_run(mock_interface_kit, mock_phidget_manager, test_args, controller):
    result = await run(test_args, None, controller)
    tasks = controller.get_scheduled_tasks()
    if tasks:
        futures = asyncio.gather(*tasks)
        while not futures.done():
            await asyncio.sleep(0)
        futures.result()
    
    await controller.stop()
    
    assert result is not None

@pytest.mark.asyncio
async def test_run_get_states(mock_interface_kit, mock_phidget_manager, controller):
    result = await run([], None, controller)
    tasks = controller.get_scheduled_tasks()
    if tasks:
        futures = asyncio.gather(*tasks)
        while not futures.done():
            await asyncio.sleep(0)
        futures.result()
    
    await controller.stop()
    
    assert result is None

@pytest.mark.asyncio
async def test_run_no_endpoint(test_event_loop, mock_interface_kit, mock_phidget_manager):
    controller = Controller(**run_config.controller_config, loop=test_event_loop)
    with pytest.raises(ValueError):
        result = await run(['set_closed', '-d', 'testdevice:bad'], test_event_loop, controller)
    
    await controller.stop()
    futures = asyncio.gather(*controller.get_scheduled_tasks())
    while not futures.done():
        await asyncio.sleep(0)


@pytest.mark.asyncio
async def test_run_wrong_command(test_event_loop, mock_interface_kit, mock_phidget_manager):
    controller = Controller(**run_config.controller_config, loop=test_event_loop)
    result = await run(['bad_command', '-d', 'unit_test_device'], test_event_loop, controller)
    
    await controller.stop()
    futures = asyncio.gather(*controller.get_scheduled_tasks())
    while not futures.done():
        await asyncio.sleep(0)
        
    futures.result()
    assert result is None


@pytest.mark.asyncio
async def test_run_no_device(test_event_loop, mock_interface_kit, mock_phidget_manager):
    controller = Controller(**run_config.controller_config, loop=test_event_loop)
    with pytest.raises(SystemExit):
        result = await run(['set_closed', '-d', 'bad_device:bad'], test_event_loop, controller)
    
    await controller.stop()
    futures = asyncio.gather(*controller.get_scheduled_tasks())
    while not futures.done():
        await asyncio.sleep(0)

@pytest.mark.asyncio
async def test_run_no_device_configurations(test_event_loop, mock_interface_kit, mock_phidget_manager, mocker):
    controller = Controller(**run_config.controller_config, loop=test_event_loop)
    mock_global = mocker.patch('builtins.globals')
    mock_global.return_value = ''
    with pytest.raises(NameError):
        result = await run(['set_closed', '-d', 'nodevice:bad'], test_event_loop, controller)
    
    await controller.stop()
    futures = asyncio.gather(*controller.get_scheduled_tasks())
    while not futures.done():
        await asyncio.sleep(0)

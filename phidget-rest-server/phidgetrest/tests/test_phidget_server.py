from phidgetrest.tests.fixtures import *
from phidgetrest.eventer import Event
# from phidgetrest.phidgetcontroller import ServerEvent, DeviceEvent, ControllerEvent
from phidgetrest import ServerEvent, DeviceEvent, ControllerEvent
from Phidgets.Events.Events import ServerConnectArgs, ServerDisconnectArgs, AttachEventArgs, DetachEventArgs, \
    ErrorEventArgs


def test_fail_init():
    with pytest.raises(AttributeError):
        Server({'fail': True})


def test_connect(server):
    assert server.connect()
    server._connected = True
    assert not server.connect()


def test_is_connected(server):
    server._connected = True
    assert server.is_connected()


def test_get_devices(server):
    assert server.get_devices() == {}


def test_check_connection(server):
    assert not server._connected
    server.check_connection()
    assert server._connected
    server.check_connection()
    assert server._connected


@pytest.mark.asyncio
async def test_server_connect_event(server, mock_caller, mock_phidget_manager):
    event_name = 'event_on_server_connect'
    setattr(server, event_name, mock_caller)
    futures = server._event(ServerConnectArgs(mock_phidget_manager))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()


@pytest.mark.asyncio
async def test_server_disconnect_event(server, mock_caller, mock_phidget_manager):
    event_name = 'event_on_server_disconnect'
    setattr(server, event_name, mock_caller)
    futures = server._event(ServerDisconnectArgs(mock_phidget_manager))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()


@pytest.mark.asyncio
async def test_device_attach_event(server, mock_caller, mock_interface_kit):
    event_name = 'event_on_device_attach'
    setattr(server, event_name, mock_caller)
    futures = server._event(AttachEventArgs(mock_interface_kit))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()


@pytest.mark.asyncio
async def test_device_detach_event(server, mock_caller, mock_interface_kit):
    serial = 1234567
    device_data = {'type': 'test_interface_kit', 'serial': str(serial)}
    server._available_devices.update({serial: device_data})
    event_name = 'event_on_device_detach'
    setattr(server, event_name, mock_caller)
    futures =  server._event(DetachEventArgs(mock_interface_kit))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()


@pytest.mark.asyncio
async def test_error_event(server, mock_caller):
    event_name = 'event_on_error'
    setattr(server, event_name, mock_caller)
    futures = server._event(ErrorEventArgs(mock_interface_kit, 'test_error', 99))
    for f in futures:
        while not f.done():
            await asyncio.sleep(0)
    mock_caller.assert_called_once()


@pytest.mark.asyncio
async def test_get_clear_errors(server, mock_caller):
    event_name = 'event_on_error'
    setattr(server, event_name, mock_caller)
    server._event(ErrorEventArgs(mock_interface_kit, 'test_error', 99))
    await asyncio.sleep(0)
    assert server.get_error()
    server.reset_errors()
    assert server.get_error() == {}


@pytest.mark.asyncio
async def test_error_event_filter(server, mock_caller):
    event_count = 20
    event_name = 'event_on_error'
    setattr(server, event_name, mock_caller)
    for i in range(event_count):
        futures = server._event(ErrorEventArgs(mock_interface_kit, 'test_error', 0))
        for f in futures:
            while not f.done():
                await asyncio.sleep(0)
    assert mock_caller.call_count == event_count


@pytest.mark.asyncio
async def test_server_unknown_event(server):
    result = server._event(ServerEvent(event='unknown_test', data={'unknown_test'}, type=14))
    assert result is None

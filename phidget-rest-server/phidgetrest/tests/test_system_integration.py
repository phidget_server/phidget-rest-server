import pytest
import asyncio
# from phidgetrest.phidgetcontroller import Controller, Server
from phidgetrest import Controller, Server
import time
from phidgetrest.tests.fixtures import test_event_loop, mock_caller
from phidgetrest.helpers import pretty_print

server_config = {
    'ip_address': '172.22.2.160',
    'server_name': 'elricsubuntuserver',
    'name': 'main_server',
    'port': 5001,
    'password': '',
    'enabled': True,
    'auto_connect': True,
}

controller_config = {
    'log_level': 'DEBUG',
    'log_disabled': False,
    'server_log_level': 'DEBUG',
    'server_log_disabled': False,
    'device_log_level': 'DEBUG',
    'device_log_disabled': False
}

test_device = 290102

device_set_command = {
    'action': 'device_command',
    'auto_add_resource': True,
    'commands': [
        {
            'action': 'set_toggle',
            'ports': [0, 2],
            'device': str(test_device),
            'start_time': time.time()
        }]}


@pytest.mark.skip
@pytest.mark.asyncio
async def test_real_manager_add_server_auto_connect(controller):
    server_data = {
        'ip_address': '172.22.2.160',
        'server_name': 'elricsubuntuserver',
        'name': 'main',
        'port': 5001,
        'password': '',
        'enabled': True,
        'auto_connect': True
    }
    
    add_server_message = {
        'action': 'add_server',
        'server_info': server_data
    }
    
    result: Server = await controller.command(add_server_message)
    while not result.is_connected():
        await asyncio.sleep(0)
    assert isinstance(result, Server)


@pytest.mark.asyncio
async def test_controller_complete(test_event_loop, mock_caller):
    add_server_message = {
        'action': 'add_server',
        'server_info': server_config
    }
    controller = Controller(**controller_config, loop=test_event_loop)
    controller.event_on_any += mock_caller
    controller.start()
    await controller.command(add_server_message)
    while test_device not in controller.get_available_devices().get('main_server'):
        await asyncio.sleep(0)
    await controller.command(device_set_command)
    tasks = controller.get_scheduled_tasks()
    futures = asyncio.gather(*tasks)
    while not futures.done():
        await asyncio.sleep(0)
    await controller.command({'action': 'stop'})
    while controller._running:
        await asyncio.sleep(0)
    assert controller.get_server_errors() == {'main_server': {}}
    # need to add a short delay to the end to accept the output change events
    time.sleep(0.1)
    mock_caller.asset_called()
    for c in mock_caller.call_args_list:
        pretty_print({c[0][0].event: c[0][0].data})

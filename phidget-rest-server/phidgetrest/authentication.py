from flask import current_app
from functools import wraps

from eve.auth import TokenAuth, BasicAuth
from eve.auth import requires_auth
from flask import request
import re

def auth_resource(resource):
    """
    decorator to allow authorisation checking using eves native endpoint rules
    :param resource: Specify the resource endpoint to look up in eve config.
    :return:
    """
    def fdec(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            return f(resource=resource, *args, **kwargs)
        return wrapped
    return fdec


def valid_ip(ip_address):
    security = current_app.data.driver.db['settings'].find_one({'name': 'security'})
    if 'localhost' in ip_address:
        return True
    
    if security is not None:
        settings = security.get('settings')
        whitelist: dict = settings.get('whitelist')
        blacklist: dict = settings.get('blacklist')
        for ip in blacklist.values():
            if re.match(ip, ip_address):
                return False
        for ip in whitelist.values():
            if re.match(ip, ip_address):
                return True
    return None
        

class RolesAuth(TokenAuth):
    def check_auth(self, token, allowed_roles, resource, method, remote_add=None):
        # use Eve's own db driver; no additional connections/resources are used
        if remote_add is None:
            remote_add = request.remote_addr
        ip_valid = valid_ip(remote_add)
        if ip_valid is not None:
            return ip_valid
        accounts = current_app.data.driver.db['accounts']
        lookup = {'token': token}
        if allowed_roles and method not in ['GET', 'PATCH']:
            # only retrieve a user if his roles match ``allowed_roles`` and is not a read or patch
            lookup['roles'] = {'$in': allowed_roles}
        account = accounts.find_one(lookup)
        return account


class AccountBasicAuth(BasicAuth):
    def check_auth(self, user, password, allowed_roles, resources, method):
        remote_add = request.remote_addr
        # ip_valid = valid_ip(remote_add)
        # if ip_valid is not None:
        #     return ip_valid
        accounts = current_app.data.driver.db['accounts']
        account = accounts.find_one({'username': user})
        if account is not None:
            return account['password'] == password
        return False

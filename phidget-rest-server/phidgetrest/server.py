import asyncio

from eve import Eve
import hashlib
import time
from flask import abort, request, json, jsonify, send_from_directory
from bson.objectid import ObjectId
from phidgetrest.settings import controller_config, WS_PORT
from phidgetrest import Controller, Server, ControllerEvent
from phidgetrest.authentication import *
from phidgetrest.validation_schemas import validate_command, InvalidUsage, error_response
from aiohttp import web
import aiohttp
from eve_swagger import swagger, add_documentation

app = Eve()
app.register_blueprint(swagger)
app.config['SWAGGER_INFO'] = {
    'title': 'Phidget Rest Interface Api',
    'version': '1.0',
    'description': 'The API related to my phidget rest service',
    'termsOfService': 'MIT',
    'contact': {
        'name': 'elric',
        'url': 'http://gitlab.com/elrichindy/phidgetrest'
    },
    'license': {
        'name': 'MIT',
        'url': 'http://gitlab.com/elrichindy/phidgetrest',
    }
}
app.config['SWAGGER_HOST'] = 'elricsubuntuserver'


phcon = Controller(**controller_config)


class WebsocketService:
    def __init__(self, port, loop=None):
        self.port = port  # type: int
        self.loop = loop  # type: asyncio.AbstractEventLoop
        self.app = None  # type: web.Application
        self.srv = None  # type: asyncio.base_events.Server
        self.auth = RolesAuth()
    
    async def start(self):
        if self.loop is None:
            self.loop = asyncio._get_running_loop()
        self.app = web.Application()
        self.app["websockets"] = []  # type: [web.WebSocketResponse]
        self.app.router.add_get("/websocket/", self._websocket_handler)
        await self.app.startup()
        handler = self.app.make_handler()
        self.srv = await asyncio.get_event_loop().create_server(handler, port=self.port)
        print("{} listening on port {}".format(self.__class__.__name__, self.port))
    
    async def close(self):
        assert self.loop is asyncio.get_event_loop()
        self.srv.close()
        await self.srv.wait_closed()
        
        for ws in self.app["websockets"]:  # type: web.WebSocketResponse
            await ws.close(code=aiohttp.WSCloseCode.GOING_AWAY, message='Server shutdown')
        
        await self.app.shutdown()
        await self.app.cleanup()
    
    async def _websocket_handler(self, request):
        assert self.loop is asyncio.get_event_loop()
        ws = web.WebSocketResponse()
        await ws.prepare(request)
        if not self.auth.check_auth(request.GET.get('token'), ['admin', 'user'], 'websocket', request.method,
                                    request.host):
            ws.send_json({"Error": "Authorisation Failed"})
            await ws.close(code=aiohttp.WSCloseCode.POLICY_VIOLATION, message='Invalid Credentials')
            return None
        
        self.app["websockets"].append(ws)
        
        await self.do_websocket(ws)
        
        self.app["websockets"].remove(ws)
        return ws
    
    async def do_websocket(self, ws: web.WebSocketResponse):
        ws.send_json(phcon.get_device_states())
        async for ws_msg in ws:  # type: aiohttp.WSMessage
             print(ws_msg)
        pass
    
    def broadcast_message(self, msg: dict):
        for ws in self.app["websockets"]:  # type: web.WebSocketResponse
            ws.send_json(msg)


def pre_accounts_callback_filter(request, lookup):
    # Filter user data so if not admin or superuser only their details are returned
    accounts = app.data.driver.db['accounts']
    token = {'token': request.authorization['username']}
    allowed_list = accounts.find_one(token)['roles']
    if set(allowed_list).isdisjoint({'admin', 'superuser'}):
        lookup.update(token)


def pre_update_accounts_callback_check(updates, original):
    # lock down to only update username and password
    invalid_fields = set(updates) - set(['username', 'password', '_updated'])
    if invalid_fields:
        abort(403, "Updating of the following field(s) {} is restricted".format(invalid_fields))
    # check if fields are different
    check_dict = dict(updates)
    
    for key, value in check_dict.items():
        if value == original[key]:
            del updates[key]
    
    if not set(updates) - set(['_updated']):
        abort(400, "No change to user details detected")
    
    # generate new token when updating
    password = updates['password'] if updates.get('password', False) else original['password']
    username = updates['username'] if updates.get('username', False) else original['username']
    updates.update({'token': generate_token(username, password)})


def post_update_accounts_callback_addition(request, payload):
    accounts = app.data.driver.db['accounts']
    account_id = {'_id': ObjectId(request.view_args['_id'])}
    token = accounts.find_one(account_id)['token']
    # modify reponse to return new_token
    res = json.loads(payload.get_data())
    res['new_token'] = token
    payload.set_data(json.dumps(res))


def generate_token(username, password):
    return hashlib.sha1('{}{}{}'.format(username, password, time.time()).encode('utf-8')).hexdigest().upper()


def add_token(documents):
    for document in documents:
        token = generate_token(document['username'], document['password'])
        document["token"] = token


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


def get_device_command(data, get_devices=False):
    """
    Gets the configuration type and device or devices to control
    :param name: name on configuration to fetch
    :return: device type and details
    """
    name = data.pop('device')
    ports = data.get('ports')
    if ports is not None:
        data.pop("ports")
    
    try:
        serial_name = int(name)
    except ValueError:
        serial_name = None
    
    devices_to_control = {}
    
    with current_app.app_context():
        device = current_app.data.driver.db['devices'].find_one({"$or": [{'name': name}, {'serial': serial_name}]})
        device_configuration = current_app.data.driver.db['deviceconfigurations'].find_one({'name': name})
        device_group = current_app.data.driver.db['devicegroups'].find_one({'name': name})
    
    device_list = []
    if device_group is not None:
        for config_name in device_group.get('configuration'):
            new_ports = []
            device_configuration = current_app.data.driver.db['deviceconfigurations'].find_one({'name': config_name})
            for serial, config in device_configuration.get('configuration').items():
                device = current_app.data.driver.db['devices'].find_one({'serial': int(serial)})
                if ports is not None:
                    if isinstance(ports,list):
                        for port in ports:
                            new_ports.extend(config.get(port))
                    elif isinstance(ports,str): 
                            new_ports.extend(config.get(ports))
                    else:
                        raise InvalidUsage("Device port is incorrect type: {}".format(type(ports)), status_code=422)
                else:
                    for p in config.values():
                        new_ports.extend(p)
                if new_ports:
                    device_list.append((device, list(set(new_ports))))
    elif device_configuration is not None:
        new_ports = []
        for serial, config in device_configuration.get('configuration').items():
            device = current_app.data.driver.db['devices'].find_one({'serial': int(serial)})
            if ports is not None:
                if isinstance(ports,list):
                    for port in ports:
                        new_ports.extend(config.get(port))
                elif isinstance(ports,str): 
                        new_ports.extend(config.get(ports))
                else:
                    raise InvalidUsage("Device port is incorrect type: {}".format(type(ports)), status_code=422)

            else:
                for p in config.values():
                    new_ports.extend(p)
            if new_ports:
                new_ports = set(new_ports)
                device_list.append((device, new_ports))
    
    elif device is not None:
        device_list.append((device, ports))
    else:
        raise InvalidUsage("No Device, Configuration or Group with that name or serial", status_code=422)
    
    for dev, new_ports in device_list:
        max_port = dev.get('outputs', 0) - 1
        if new_ports is not None:
            try:
                ports_list = [int(port) for port in new_ports]
                if min(ports_list) < 0 or max(ports_list) > max_port:
                    error = error_response("Device Port Out Of Range",
                                           "One of the ports in {} is out of range 0-{} for device {}".format(ports,
                                                                                                              max_port,
                                                                                                              device.get(
                                                                                                                      "name")),
                                           code=422)
                    raise InvalidUsage(error, 422)
            except TypeError:
                raise InvalidUsage("All ports {0} must be a valid integers".format(ports),
                                   status_code=422)
        
        else:
            ports_list = [port for port in range(max_port + 1)]
        serial_name = int(device.get('serial'))
        if serial_name in devices_to_control:
            p = devices_to_control.pop(serial_name)
            p.extend(ports_list)
            p = list(set(p))
            devices_to_control[serial_name] = p
        else:
            devices_to_control[serial_name] = ports_list
    
    if get_devices:
        return devices_to_control
    
    start_time = data.get('start_time', time.time())
    
    commands = []
    device_command = {
        'action': 'device_command',
        "commands": commands
    }
    
    for serial, ports in devices_to_control.items():
        command = {'device': str(serial),
                   'start_time': start_time,
                   'ports': ports
                   }
        command.update(data)
        commands.append(command)
    
    error, normalised_command = validate_command(device_command)
    if error:
        raise InvalidUsage(error, status_code=400)
    
    return normalised_command


@app.route('/tasks', methods=['POST', 'GET'])
@auth_resource('roles_auth')
@requires_auth('resource')
def tasks(*args, **kwargs):
    if request.method == 'GET':
        task = asyncio.run_coroutine_threadsafe(phcon.command({'action':'get_tasks'}), loop=loop)
        try:
            task_details, task_summary = task.result()
        except ValueError:
            task_summary = task.result()
        return json.dumps(task_summary)

    data: dict = json.loads(request.get_data())
    command = {'action': data.get('action'), 'uuid':data.get('uuid')}
    task = asyncio.run_coroutine_threadsafe(phcon.command(command), loop=loop)
    try:
        task_details, task_summary = task.result()
    except ValueError:
        task_summary = task.result()
    return json.dumps(task_summary)

@app.route('/control', methods=['POST', 'GET'])
@auth_resource('roles_auth')
@requires_auth('resource')
def control(*args, **kwargs):
    if request.method == 'GET':
        return json.dumps(phcon.get_device_states())
    
    data: dict = json.loads(request.get_data())
    command = get_device_command(data)
    task = asyncio.run_coroutine_threadsafe(phcon.command(command), loop=loop)
    try:
        task_details, task_summary = task.result()
    except ValueError:
        task_summary = task.result()
    return json.dumps(task_summary)


@app.route('/control/<device>', methods=['POST', 'GET'])
@auth_resource('roles_auth')
@requires_auth('item')
def control_device(*args, **kwargs):
    raw_data = request.get_data()
    if raw_data:
        data = json.loads(request.get_data())
    else:
        data = {}
    device = kwargs.get('device')
    data.update({"device": device})
    
    if request.method == 'GET':
        devices = get_device_command(data, get_devices=True)
        states = phcon.get_device_states()
        response = {}
        for serial, ports in devices.items():
            if serial not in states:
                continue
            state_info = states.get(serial)
            filtered = {}
            for port in ports:
                if port in state_info:
                    filtered.update({port: state_info.get(port)})
            if serial in response:
                response[serial].update(filtered)
            else:
                response[serial] = filtered
        return json.dumps(response)
    
    command = get_device_command(data)
    task = asyncio.run_coroutine_threadsafe(phcon.command(command), loop=loop)

    try:
        task_details, task_summary = task.result()
    except ValueError:
        task_summary = task.result()
    return json.dumps(task_summary)


@app.route('/control/<device>/<ports>', methods=['POST', 'GET'])
@auth_resource('roles_auth')
@requires_auth('item')
def control_device_port(*args, **kwargs):
    raw_data = request.get_data()
    if raw_data:
        data = json.loads(request.get_data())
    else:
        data = {}
    
    device = kwargs.get('device')
    ports = kwargs.get('ports').split(':')
    data.update({"device": device, "ports": ports})
    
    if request.method == 'GET':
        devices = get_device_command(data, get_devices=True)
        states = phcon.get_device_states()
        response = {}
        for serial, ports in devices.items():
            if serial not in states:
                continue
            state_info = states.get(serial)
            filtered = {}
            for port in ports:
                if port in state_info:
                    filtered.update({port: state_info.get(port)})
            response.update({serial: filtered})
        return json.dumps(response)
    
    command = get_device_command(data)
    task = asyncio.run_coroutine_threadsafe(phcon.command(command), loop=loop)
    try:
        task_details, task_summary = task.result()
    except ValueError:
        task_summary = task.result()
    return json.dumps(task_summary)


@app.route('/control/<device>/<ports>/<action>', methods=['POST'])
@auth_resource('roles_auth')
@requires_auth('item')
def control_device_action(*args, **kwargs):
    raw_data = request.get_data()
    if raw_data:
        data = json.loads(raw_data)
    else:
        data = {}
    device = kwargs.get('device')
    ports = kwargs.get('ports').split(':')
    action = kwargs.get('action')
    data.update({"device": device, "ports": ports, "action": action})
    
    command = get_device_command(data)
    task = asyncio.run_coroutine_threadsafe(phcon.command(command), loop=loop)
    try:
        task_details, task_summary = task.result()
    except ValueError:
        task_summary = task.result()
    return json.dumps(task_summary)
    


# recover token endpoint
@app.route('/recover/<user>', methods=['GET'])
@auth_resource('basic_auth')
@requires_auth('item')
def recover_user_token(*args, **kwargs):
    accounts = current_app.data.driver.db['accounts']
    account = accounts.find_one({'username': kwargs.get('user')})
    token = account.get('token')
    return json.dumps({'token': token})


def post_add_server(req, res):
    # only processs if successful command
    if res.status_code == 201:
        data = req.json
        connect_server(data)


def device_attached_event(data: ControllerEvent):
    with current_app.app_context():
        serial = data.data.get('serial')
        coll = app.data.driver.db['devices']
        config = coll.find_one({'serial': data.data.get('serial')})
        if config is None:
            coll.save({"name": str(serial),
                       "outputs": data.data.get("outputs", -1),
                       "serial": serial,
                       "connected": True})
        else:
            coll.update({'serial': serial}, {"$set": {"connected": True}})


def device_detach_event(data: ControllerEvent):
    with current_app.app_context():
        serial = data.data.get('serial')
        coll = app.data.driver.db['devices']
        config = coll.find_one({'serial': data.data.get('serial')})
        if config is not None:
            coll.update({'serial': serial}, {"$set": {"connected": False}})


async def reconnect_servers():
    # wait for no scheduled tasks
    print('Waiting for tasks to complete')
    while phcon.get_scheduled_tasks():
        await asyncio.sleep(1)
    
    # disconnect all servers
    print('disconnecting servers')
    phcon.disconnect()
    
    # re-connect all servers
    print('reconnecting servers')
    with app.app_context():
        server_collection = app.data.driver.db['servers']
        servers = server_collection.find({})
        for s in servers:
            connect_server(s)


def post_patch_server(req, res):
    print('Patching servers status {}'.format(res.status))
    asyncio.run_coroutine_threadsafe(reconnect_servers(), loop=loop)


def post_get_servers(req, resp):
    """
    Updates the server return object with devices currently connected to the server
    :param req: request data object
    :param resp: response obj
    :return:
    """
    res = json.loads(resp.get_data())
    name = req.view_args.get('name')
    if name is None:
        res['available_devices'] = phcon.get_available_devices()
    else:
        res['available_devices'] = phcon.get_available_devices(name)
    resp.set_data(json.dumps(res))


def device_configuration_validate(items):
    devices = current_app.data.driver.db['devices']
    groups = current_app.data.driver.db['devicegroups']
    invalid_names = [x.get('name') for x in devices.find({})]
    invalid_names.extend([x.get('name') for x in groups.find({})])
    for item in items:
        config: dict = item.get('configuration')
        name = item.get('name')
        if name in invalid_names:
            error = error_response("Device Name Already Used", "{} already used in group or device name".format(name),
                                   422)
            raise InvalidUsage(error, 422)
        serial_check = None
        for serial in config.keys():
            serial_check = serial
            try:
                doc = devices.find_one({'serial': int(serial)})
            except ValueError:
                error = error_response("Device Key Invalid", "{} is not a Valid Integer".format(serial), 422)
                raise InvalidUsage(error, 422)
            if doc is not None:
                ports = set([x for y in config.get(serial).values() for x in y])
                max_outputs = doc.get('outputs', 0) - 1
                if min(ports) < 0 or max(ports) > max_outputs:
                    error = error_response("Device Port Out Of Range",
                                           "One of the ports in {} is out of range".format(ports), 422)
                    raise InvalidUsage(error, 422)
                break
        else:
            error = error_response("Device Key Not Found",
                                   "Device {} is not in Device Database".format(serial_check), 422)
            raise InvalidUsage(error, 422)


def device_groups_validate(items):
    device_configurations = current_app.data.driver.db['deviceconfigurations']
    devices = current_app.data.driver.db['devices']
    invalid_names = [x.get('name') for x in devices.find({})]
    invalid_names.extend([x.get('name') for x in device_configurations.find({})])
    for item in items:
        config: dict = item.get('configuration')
        name = item.get('name')
        if not config:
            error = error_response("Configuration is Empty", code=422)
            raise InvalidUsage(error, 422)
        if name in invalid_names:
            error = error_response("Device Name Already Used",
                                   "{} already used in device configurations or device name".format(name), 422)
            raise InvalidUsage(error, 422)
        
        for config_name in config:
            doc = device_configurations.find_one({'name': config_name})
            if doc is None:
                error = error_response("Device Key Not Found",
                                       "Device '{}' is not in Device Configurations".format(config_name), 422)
                raise InvalidUsage(error, 422)


def connect_server(data: dict):
    name = data.get('name')
    server_name = data.get('server_name')
    port = int(data.get('address_port').get('port'))
    password = data.get('password')
    ip_address = data.get('address_port').get('address')
    server_info = {
        'ip_address': ip_address,
        'name': name,
        'server_name': server_name,
        'port': port,
        'password': password,
        'enabled': True,
        'auto_connect': True
    }
    add_server_command = {
        'action': 'add_server',
        'server_info': server_info
    }
    asyncio.run_coroutine_threadsafe(phcon.command(add_server_command), loop=loop)


websocket = WebsocketService(WS_PORT)


def debug_cb(e):
    with current_app.app_context():
        print(e.data)
        websocket.broadcast_message(e.data)
        # pretty_print(e.data)


async def main():
    future = loop.run_in_executor(None, app.run, '0.0.0.0')
    # connect to servers saved in db
    with app.app_context():
        phcon.event_on_device_attach += device_attached_event
        phcon.event_on_device_detach += device_detach_event
        phcon.event_on_any += debug_cb
        phcon.start()
        print("Starting Websocker Server")
        ws_future = asyncio.ensure_future(websocket.start(), loop=loop)
        server_collection = current_app.data.driver.db['servers']
        servers = server_collection.find({})
        for s in servers:
            connect_server(s)
        
        await future
        while True:
            await asyncio.sleep(1)


if __name__ == '__main__':
    app.on_insert_accounts += add_token
    app.on_insert_deviceconfigurations += device_configuration_validate
    app.on_insert_devicegroups += device_groups_validate
    app.on_pre_GET_accounts += pre_accounts_callback_filter
    app.on_update_accounts += pre_update_accounts_callback_check
    app.on_post_PATCH_accounts += post_update_accounts_callback_addition
    app.on_post_POST_servers += post_add_server
    app.on_post_PATCH_servers += post_patch_server
    app.on_post_GET_servers += post_get_servers
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        print('Exiting Server...')
        

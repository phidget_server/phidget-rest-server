# -*- coding: utf-8 -*-
from future.utils import iteritems
import re

"""
    Events
    ~~~~~~

    Implements C#-Style Events.

    Derived from the original work by Zoran Isailovski:
    Added better handling of adding events with the __events__ dict added to classes so errors are generated on
    the adding of events rather then failing later in code if typed incorrectly
    http://code.activestate.com/recipes/410686/ - Copyright (c) 2005

    :copyright: (c) 2014-2017 by Nicola Iarocci.
    :license: BSD, see LICENSE for more details.
"""


# class EventsException(Exception):
#     pass


# class EventData(object):
#         pass


class Event(object):
    """
    A Generic event object, can be subclassed to provide expected objects with kwargs for different applications
    and better documentation
    """
    def __init__(self, *args, **kwargs):
        for key, value in iteritems(kwargs):
            # if isinstance(value, dict):
            #     # setattr(self, key, EventData())
            #     # new_obj = getattr(self, key)
            #     for k, v in iteritems(value):
            #         setattr(self, k, v)
            # else:
            setattr(self, key, value)


class Events:
    """
    Encapsulates the core to event subscription and event firing, and feels
    like a "natural" part of the language.

    The class Events is there mainly for 3 reasons:

        - Events (Slots) are added automatically, so there is no need to
        declare/create them separately. This is great for prototyping. (Note
        that `__events__` is optional and should primarily help detect
        misspelled event names.)
        - To provide (and encapsulate) some level of introspection.
        - To "steel the name" and hereby remove unneeded redundancy in a call
        like:

            xxx.OnChange = event('OnChange')
    """
    
    def __getattr__(self, name, *default, **kwargs):
        if name.startswith('__'):
            raise AttributeError("type object '%s' has no attribute '%s'" %
                                 (self.__class__.__name__, name))
        
        if hasattr(self, '__events__'):
            if re.search(name, ''.join(self.__events__)) is None:
                raise AttributeError('Not supported event name: {0}'.format(name))
            
            
        self.__dict__[name] = ev = _EventSlot(name)
        return ev
    
    def __repr__(self):
        return '<%s.%s object at %s>' % (self.__class__.__module__,
                                         self.__class__.__name__,
                                         hex(id(self)))
    
    __str__ = __repr__
    
    def __len__(self):
        return len(self.__dict__.items())
    
    def __iter__(self):
        def gen(dictitems=self.__dict__.items()):
            for attr, val in dictitems:
                # if isinstance(val, _EventSlot):
                #     yield val
                yield val
            
        return gen()


class _EventSlot:
    def __init__(self, name):
        self.targets = []
        self.__name__ = name
    
    def __repr__(self):
        return "event '%s'" % self.__name__
 
    def __call__(self, *a, **kw):
        for f in tuple(self.targets):
            f(*a, **kw)

    def __iadd__(self, f):
        self.targets.append(f)
        return self

    def __isub__(self, f):
        while f in self.targets:
            self.targets.remove(f)
        return self

    def __len__(self):
        return len(self.targets)

    def __iter__(self):
        def gen():
            for target in self.targets:
                yield target
        
        return gen()

    def __getitem__(self, key):
        return self.targets[key]

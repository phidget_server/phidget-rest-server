import os
import sys

# import traceback
# import uuid
from Phidgets.PhidgetException import PhidgetException
from Phidgets.Devices import InterfaceKit
from Phidgets import Manager
# from Phidgets.Phidget import PhidgetLogLevel
from Phidgets import Phidget
from Phidgets.Events.Events import *
import asyncio
# import random
import time
import datetime
# import pprint

# local imports
import baselogger
from phidgetrest import Scheduler

# eventhandling
from phidgetrest import eventer
import math


class DeviceEvent(eventer.Event):
    OUTPUT_CHANGE = 0
    CYCLE_START = 1
    CYCLE_END = 2
    CYCLE_UPDATE = 3
    ATTACH = 4
    DETACH = 5
    SET_START = 6
    SET_END = 7
    DEVICE_ERROR = 9
    event = None
    data = None
    type = None


class Device(eventer.Events):
    def __init__(self, data: dict, log_level='INFO', loop=None, **kwargs):
        # init required defaults here as can add configurations to kwargs after
        if loop is None:
            self._loop = asyncio._get_running_loop()
        else:
            self._loop = loop
        
        self.__events__ = ('event_on_output_change', 'event_on_error', 'event_on_cycle_start', 'event_on_cycle_end',
                           'event_on_any', 'event_on_device_attach', 'event_on_device_detach', 'event_on_cycle_update',
                           'event_on_set_start', 'event_on_set_end')
        self.configurations = {}
        self.analogue_inputs = None
        self.digital_inputs = None
        self._digital_outputs_states = {}
        self.digital_outputs = None
        self._ready = False
        self._running_tasks = {}
        self.serial = 0
        self.address = ''
        self.password = b''
        self.port = 5001
        for key, value in data.items():
            # strings must be bytes objects
            setattr(self, key, value)
        
        # can override defaults from data if required
        for key, value in kwargs.items():
            setattr(self, key, value)
        
        self._logger = baselogger.Logger('device:{0}'.format(getattr(self, 'serial')), log_level, 'phidget_device.log')
        self._logger.disabled = getattr(self, 'log_disabled', False)
        # for future support, currently disabled
        # if data.get('type') == 'PhidgetInterfaceKit':
        self.interfacekit = InterfaceKit.InterfaceKit()
        self.interfacekit.setOnOutputChangeHandler(self._event)
        self.interfacekit.setOnAttachHandler(self._event)
        self.interfacekit.setOnDetachHandler(self._event)
        self.interfacekit.setOnErrorhandler(self._event)
        if data.get('auto_connect', True):
            self.open()
    
    def open(self):
        self._logger.file_console.info('Opening Interface Kit {0}'.format(getattr(self, 'serial')))
        self.interfacekit.openRemoteIP(self.address.encode(), self.port, self.serial, self.password)
    
    def close(self):
        self.interfacekit.closePhidget()
    
    async def run_command(self, command: dict):
        # handles waiting for the device to be ready
        if not self._ready:
            # wait up to a second for device to be ready
            for i in range(10):
                await asyncio.sleep(0.1)
                if self._ready:
                    break
            else:
                raise IOError("Device {} not ready".format(self.serial))
        
        data = command.get('data')
        action = data.get('action', None)
        if action is None:
            return False
        elif action in ['set_closed', 'set_open', 'set_toggle']:
            event_info = {
                'uuid': command.get('uuid'),
                'event': 'event_on_set_start'
            }
            self._event(DeviceEvent(event=event_info.get('event'), data=event_info, type=DeviceEvent.SET_START))
            result = await self._change_port(data.get('ports'), action)
            event_info.update({'event':'event_on_set_end'})
            self._event(DeviceEvent(event=event_info.get('event'), data=event_info, type=DeviceEvent.SET_END))
        elif action == 'cycle':
            result = await self._run_cycles(data.get('ports'), data.get('cycles'), data.get('time_open'),
                                            data.get('time_closed'), state_start=data.get('state_start', 'set_closed'),
                                            state_end=data.get('state_end', 'set_closed'),
                                            id=command.get('uuid'))
        else:
            return False
        
        return result
    
    async def _run_cycles(self, ports: list, cycles, time_on, time_off, id, state_start='set_closed',
                          state_end='set_closed'):
        # create generator for times
        def generate_times(times):
            if isinstance(times, list):
                time_genertor = (t for t in times)
                total = sum(times)
                cycle_count = len(times)
            else:
                time_genertor = (times for n in range(cycles))
                total = sum([times for n in range(cycles)])
                cycle_count = cycles
            
            return time_genertor, total, cycle_count
        
        times_closed, total_time_closed, cycle_count = generate_times(time_on)
        times_open, total_time_open, cycle_count = generate_times(time_off)
        times = zip(times_closed, times_open)
        
        current_time = datetime.datetime.utcnow()
        start_time = time.time()
        total_run_time = total_time_closed + total_time_open
        end_time = current_time + datetime.timedelta(seconds=total_run_time)
        data = {id: {'start_time': current_time.isoformat(),
                     'run_time': math.floor(total_run_time * 100) / 100,
                     'time_left': total_run_time,
                     'expected_end_time': end_time.isoformat(),
                     'serial': self.serial,
                     'ports': ports,
                     'total_cycles': cycle_count,
                     'cycle': 0}}
        self._running_tasks.update(data)
        self._logger.file_console.debug('Task Started {0}: details {1}'.format(id, self._running_tasks.get(id)))
        self._event(DeviceEvent(event='event_on_cycle_start', data=data, type=DeviceEvent.CYCLE_START))
        await self._change_port(ports, state_start)
        # await asyncio.sleep(0.2)
        
        run_time = 0
        current_cycle = 0
        for time_closed, time_open in times:
            current_cycle += 1
            data[id]['cycle'] = current_cycle
            await self._change_port(ports, 'set_closed')
            next_time = time_closed - (time.time() - start_time - run_time)
            data[id]['time_left'] = math.floor((total_run_time - run_time) * 100) / 100
            run_time += time_closed
            data[id]['current_cycle_time'] = time_closed + time_open
            data[id]['state'] = 'closed'
            data[id]['closed_cycle_duration'] = time_closed
            data[id]['open_cycle_duration'] = time_open
            self._event(
                    DeviceEvent(event='event_on_cycle_update', data=data, type=DeviceEvent.CYCLE_UPDATE))
            await asyncio.sleep(0 if next_time < 0 else next_time)
            await self._change_port(ports, 'set_open')
            next_time = time_open - (time.time() - start_time - run_time)
            time_left = math.floor((total_run_time - run_time) * 100) / 100
            data[id]['time_left'] = 0 if time_left < 0 else time_left
            run_time += time_open
            data[id]['state'] = 'open'
            self._event(
                    DeviceEvent(event='event_on_cycle_update', data=data, type=DeviceEvent.CYCLE_UPDATE))
            await asyncio.sleep(0 if next_time < 0 else next_time)
        
        await self._change_port(ports, state_end)
        self._logger.file_console.debug('task completed {0}'.format(self._running_tasks.pop(id)))
        data[id]['finished'] = datetime.datetime.utcnow().isoformat()
        data[id]['actual_run_time'] = math.ceil((time.time() - start_time) * 1000) / 1000
        self._event(DeviceEvent(event='event_on_cycle_end', data=data, type=DeviceEvent.CYCLE_END))
        return data
    
    async def _change_port(self, ports: list, action: str):
        self._logger.file_console.debug(
                'Changing ports {0} with action {1} on device {2}'.format(ports, action, self.serial))
        for port in ports:
            if port > self.digital_outputs - 1 or port < 0:
                raise PhidgetException(99)
            if action == 'set_closed':
                self.interfacekit.setOutputState(port, True)
            elif action == 'set_open':
                self.interfacekit.setOutputState(port, False)
            elif action == 'set_toggle':
                state = not self.interfacekit.getOutputState(port)
                self.interfacekit.setOutputState(port, state)
            else:
                self._logger.file_console.error('change port action {} not supported'.format(action))
                return False
    
    def get_output_state(self):
        return self._digital_outputs_states
    
    def _event(self, e):
        if isinstance(e, OutputChangeEventArgs):
            state = 'open' if e.state is False else 'closed'
            self._logger.file_console.debug('Device {0}, output {1} is {2}'.format(self.serial, e.index, state))
            event_name = 'event_on_output_change'
            data = {'serial': self.serial, 'index': e.index, 'state': state}
            event_type = DeviceEvent.OUTPUT_CHANGE
            self._digital_outputs_states.update({e.index: e.state})
        
        elif isinstance(e, AttachEventArgs):
            self.digital_outputs = self.interfacekit.getOutputCount()
            self._ready = True
            self._logger.file_console.debug(
                    'Device serial {0} and {1} outputs has Attached'.format(getattr(self, 'serial'),
                                                                            self.digital_outputs))
            event_name = 'event_on_device_attach'
            data = {'serial': self.serial, 'outputs': self.digital_outputs}
            event_type = DeviceEvent.ATTACH
        elif isinstance(e, DetachEventArgs):
            self._logger.file_console.warning('Device {} has Detached'.format(self.serial))
            self._ready = False
            event_name = 'event_on_device_detach'
            data = {'serial': self.serial}
            event_type = DeviceEvent.DETACH
        elif isinstance(e, DeviceEvent):
            if e.type == DeviceEvent.CYCLE_START:
                info: dict = list(e.data.values())[0]
                self._logger.file_console.debug(
                        'Device {} Cycle Started, duration {}'.format(self.serial, info.get('run_time')))
                event_name = e.event
                data = e.data
                event_type = e.type
            elif e.type == DeviceEvent.CYCLE_END:
                info = list(e.data.values())[0]
                self._logger.file_console.debug(
                        'Device {} Cycle Ended at {}'.format(self.serial, info.get('finished')))
                event_name = e.event
                data = e.data
                event_type = e.type
            elif e.type == DeviceEvent.CYCLE_UPDATE:
                info = list(e.data.values())[0]
                self._logger.file_console.debug(
                        'Device {} Cycle updated, current cycle {}, state {}'.format(self.serial, info.get('cycle'),
                                                                                     info.get('state')))
                event_name = e.event
                data = e.data
                event_type = e.type
            elif e.type in [DeviceEvent.SET_END, DeviceEvent.SET_START]:
                event_name = e.event
                data = e.data
                event_type = e.type
            else:
                self._logger.file_console.error('Unknown Device Event {}'.format(e.type))
                return
        elif isinstance(e, ErrorEventArgs):
            self._logger.file_console.error('Device Error {0}: {1}'.format(e.eCode, e.description))
            event_name = 'event_on_error'
            data = {'code': e.eCode, 'description': e.description}
            event_type = DeviceEvent.DEVICE_ERROR
        else:
            self._logger.file_console.warning('Got Unknown Event: {0}'.format(e))
            return
        
        # schedule events to run in loop to prevent thread issues
        futures = []
        futures.append(
                asyncio.run_coroutine_threadsafe(self._trigger_callback(event_name, data, event_type), loop=self._loop))
        futures.append(
                asyncio.run_coroutine_threadsafe(self._trigger_callback('event_on_any', data, event_type),
                                                 loop=self._loop))
        return futures
    
    async def _trigger_callback(self, ename, edata, etype):
        events = getattr(self, ename)
        events(DeviceEvent(event=ename, data=edata, type=etype))


class ServerEvent(eventer.Event):
    SERVER_CONNECT = 0
    SERVER_DISCONNECT = 1
    DEVICE_ATTACH = 2
    DEVICE_DETACH = 3
    SERVER_ERROR = 9
    event = None
    data = None
    type = None


class Server(eventer.Events):
    def __init__(self, data: dict, loop=None, auto_reconnect_interval=5.0, **kwargs):
        self.__events__ = ('event_on_server_connect', 'event_on_server_disconnect', 'event_on_device_attach',
                           'event_on_device_detach', 'event_on_error', 'event_on_any')
        self._error = {}
        self.data = data
        self._auto_reconnect_interval = auto_reconnect_interval
        if loop is None:
            self._loop = asyncio._get_running_loop()
        else:
            self._loop = loop
        
        for key, value in data.items():
            # strings must be bytes objects
            if isinstance(value, str):
                value = value.encode('utf-8')
            setattr(self, key, value)
        
        # can override defaults from data if required
        for key, value in kwargs.items():
            setattr(self, key, value)
        
        # check for minimum required options
        if not hasattr(self, 'port') or not hasattr(self, 'ip_address'):
            raise AttributeError('Missing keys! make sure ip_address and port are supplied')
        
        self.devices = {}
        self._available_devices = {}  # type: dict{dict}
        self._connected = False
        
        # configure logging
        self._logger = baselogger.Logger('PhidgetServer', getattr(self, 'log_level', 'INFO'), 'phidget_server.log')
        self._logger.disabled = getattr(self, 'log_disabled', False)
        
        self.manager = Manager.Manager()
        
        # set Phidget handlers
        self.manager.setOnAttachHandler(self._event)
        self.manager.setOnDetachHandler(self._event)
        self.manager.setOnErrorHandler(self._event)
        self.manager.setOnServerConnectHandler(self._event)
        self.manager.setOnServerDisconnectHandler(self._event)
    
    def disconnect(self):
        self.manager.closeManager()
    
    def connect(self):
        if not self._connected:
            self.manager.openRemoteIP(getattr(self, 'ip_address'),
                                      int(getattr(self, 'port')),
                                      getattr(self, 'password', ''))
            return True
        return False
    
    def __get_device_details(self, device: Phidget.Phidget):
        device_info = {'class': device.getDeviceClass(), 'id': device.getDeviceID(), 'label': device.getDeviceLabel(),
                       'name': device.getDeviceName(), 'type': device.getDeviceType(),
                       'version': device.getDeviceVersion(), 'address': device.getServerAddress(),
                       'serial': device.getSerialNum(), 'outputs': int(device.getDeviceName().split("/")[-1]),
                       'port': getattr(self, 'port')}
        return device_info
    
    def get_available_devices(self):
        """
        Gets a list of available devices
        :return: Dict containing dict's of available devices
        :rtype: dict
        """
        return self._available_devices
    
    def get_devices(self):
        return self.devices
    
    def is_connected(self):
        return self._connected
    
    def check_connection(self):
        attached = self.manager.isAttachedToServer()
        # would show an error in the auto handling connections states
        if attached != self._connected:
            # correct the error
            self._logger.file_console.warning('Connection State Changed but not caught, was {0} now {1}'
                                              .format(self._connected, attached))
            self._connected = attached
    
    def get_error(self):
        return self._error
    
    def reset_errors(self):
        self._error.clear()
    
    def _event(self, e):
        if isinstance(e, AttachEventArgs):
            device = e.device  # type: Phidget.Phidget
            data = self.__get_device_details(device)
            self._logger.file_console.info('Device {0} serial {1} connected'.format(data.get('type', 'unknown'),
                                                                                    data.get('serial')))
            self._available_devices[data.get('serial')] = data
            event_name = 'event_on_device_attach'
            event_type = ServerEvent.DEVICE_ATTACH
        elif isinstance(e, DetachEventArgs):
            device = e.device  # type: Phidget.Phidget
            data = self._available_devices.pop(device.getSerialNum())  # type: dict
            self._logger.file_console.warning('Device {0} serial {1} disconnected'.format(data.get('type', 'unknown'),
                                                                                          data.get('serial')))
            event_name = 'event_on_device_detach'
            event_type = ServerEvent.DEVICE_DETACH
        elif isinstance(e, ServerConnectArgs):
            self._connected = True
            self._logger.file_console.info('Server {0} now Connected on {1}:{2}'.format(self.data.get('server_name'),
                                                                                        self.data.get('ip_address'),
                                                                                        self.data.get('port')))
            event_name = 'event_on_server_connect'
            data = self.data
            event_type = ServerEvent.SERVER_CONNECT
        elif isinstance(e, ServerDisconnectArgs):
            self._connected = False
            self._logger.file_console.warning(
                    'Server {0} Disconnected from {1}:{2}'.format(self.data.get('server_name'),
                                                                  self.data.get('ip_address'),
                                                                  self.data.get('port')))
            event_name = 'event_on_server_disconnect'
            data = self.data
            event_type = ServerEvent.SERVER_DISCONNECT
        elif isinstance(e, ErrorEventArgs):
            iso_time_stamp = datetime.datetime.utcnow().isoformat()
            error_code_string = str(e.eCode)
            error_message = e.description
            # pop item from dict to update it or get new item
            data = self._error.pop(error_code_string, {'first_reported': iso_time_stamp})
            data[error_message] = data.get(error_message, 0) + 1
            data['last_reported'] = iso_time_stamp
            # insert in to dict
            self._error[error_code_string] = data
            error_count = data.get(error_message)
            
            # filter error to decrease spam should log max of 5, only used is logging enabled to increase speed
            # removed as added for increasing speed when many errors, can include back if any noticeable speed issues.
            # if not self.logger.disabled:
            if (error_count == 1 or not error_count % 20) and error_count < 100:
                self._logger.file_console.error('code {0}: "{1}" has occurred {2} time(s)'.format(e.eCode,
                                                                                                  e.description,
                                                                                                  error_count))
                if error_count >= 20:
                    self._logger.file_console.error(
                            'Disconnecting server {} {}:{} due to too many errors'.format(self.name,
                                                                                          self.ip_address, self.port))
                    self.disconnect()
            # todo: may need to add handling of different errors
            #  placeholder for beginning but currently just set connection to false as is hte command issue
            if e.eCode:
                self._connected = False
            
            event_name = 'event_on_error'
            event_type = ServerEvent.SERVER_ERROR
        else:
            self._logger.file_console.warning('Got Unknown Event {0}'.format(e))
            return
        
        # schedule events to run in loop to prevent thread issues
        futures = []
        futures.append(
                asyncio.run_coroutine_threadsafe(self._trigger_callback(event_name, data, event_type), loop=self._loop))
        futures.append(
                asyncio.run_coroutine_threadsafe(self._trigger_callback('event_on_any', data, event_type),
                                                 loop=self._loop))
        return futures
    
    async def _trigger_callback(self, ename, edata, etype):
        events = getattr(self, ename)
        events(ServerEvent(event=ename, data=edata, type=etype))


class ControllerEvent(eventer.Event):
    """
    
    """
    event = None
    data = None
    type = None


class Controller(eventer.Events):
    def __init__(self, **kwargs):
        self._loop = kwargs.get('loop', asyncio.get_event_loop())
        self._event_queue = kwargs.get('queue', asyncio.Queue())
        self._task_queue = {}
        self.log_level = 'DEBUG'
        self.log_disabled = False
        self.__events__ = ['event_on_server_connect', 'event_on_server_disconnect',
                           'event_on_device_attach', 'event_on_device_detach', 'event_on_error',
                           'event_on_any', 'event_on_cycle_start', 'event_on_cycle_end',
                           'event_on_output_change', 'event_on_cycle_update', 'event_on_set_start', 'event_on_set_end']
        
        self._servers = {}
        self._devices = {}
        self._devices_states = {}
        # self.args = args
        self._all_connected = False
        self._connection_status = {}
        self._connection_timer = None
        self._running = False
        
        for key, value in kwargs.items():
            setattr(self, key, value)
        
        self._scheduler = Scheduler(loop=self._loop, callback=self.run_device_command, autostart=False,
                                    loglevel=self.log_level, log_disabled=self.log_disabled)
        self.scheduler_task = None
        self._logger = baselogger.Logger('PhidgetController', self.log_level, 'phidget_controller.log')
        self._logger.disabled = self.log_disabled
    
    async def schedule_task(self, tasks, auto_add_resource):
        """
        pulls data from task to add to scheduler returns results
        :param auto_add_resource: Automatically add requested resources
        :param tasks: task to perform
        :type tasks: dict
        :return: task details if success or error details if not
        :rtype: typing.Coroutine[list[dict]]
        """
        # generate task for each resource/ports defined
        tasks_for_each_resource = []
        for task in tasks:
            command_type = task.get('action')
            # get resources
            resources = []
            for port in task.get('ports'):
                resources.append('{0}:{1}'.format(task.get('device'), port))
            
            # common commands
            start_time = task.get('start_time', time.time())
            
            if command_type == 'cycle':
                cycles = task.get('cycles', 1)
                time_on = task.get('time_open', 1)
                time_off = task.get('time_closed', 1)
                duration = 0.0
                if isinstance(time_on, list):
                    duration += sum(time_on)
                else:
                    duration += time_on * cycles
                
                if isinstance(time_off, list):
                    duration += sum(time_off)
                else:
                    duration += time_off * cycles
            
            elif 'set' in command_type:
                # must have some duration to account for time taken to run task
                duration = 0.1
            else:
                return {'error': 'task not supported'}
            
            # for resource in resources:
            #     tasks_for_each_resource.append({resource: {'start': start_time,
            #                                                'duration': duration,
            #                                                'data': task}})
            
            tasks_for_each_resource.append({'resources': resources,
                                            'data': {'start': start_time,
                                                     'duration': duration,
                                                     'data': task}})
        return await self._scheduler.add_task(tasks_for_each_resource, auto_add_resources=auto_add_resource)
    
    def start(self):
        self._running = True
        self.scheduler_task = self._scheduler.start()
    
    async def stop(self):
        await self._scheduler.stop()
        self.disconnect()
        self._running = False
    
    async def command(self, command: dict):
        action = command.get('action')
        if action == 'stop':
            await self.stop()
            return True
        elif action == 'add_server':
            server_info = command.get('server_info', None)
            return await self.add_server(**server_info)
        elif action == 'add_device':
            device_info = command.get('device_info', None)
            return await self.add_device(**device_info)
        elif action == 'cancel_task':
            task_id = command.get('uuid')
            result = self._scheduler.cancel_task(task_id)
            return result
        elif action == 'get_tasks':
            result = self._scheduler.get_running_tasks(coros=False)
            return result
        elif action == 'device_command':
            commands = command.get('commands', None)
            # confirm device exists:
            for c in commands:
                availble_devices = [y for x in self.get_available_devices().values() for y in x.keys()]
                if int(c.get('device')) not in availble_devices:
                    return {'error': 'device {} is not available'.format(c.get('device'))}
            
            return await self.schedule_task(commands, auto_add_resource=command.get('auto_add_resource', False))
        else:
            return {'error': 'command not supported'}
    
    def get_server_errors(self, *server_names: str):
        """
        Gets and return the error from a defined server
        :param server_names: tuple of server names
        :return: list of dicts with error details
        :rtype : list[dict]
        """
        error_dict = {}
        # remove duplicate requests
        if server_names:
            s_names = set(server_names)
            for server_name in s_names:
                server_obj = self._servers.get(server_name, None)  # type: Server
                if server_obj is not None:
                    error_dict[server_name] = server_obj.get('device').get_error()
                else:
                    return {'error': 'no server named {}'.format(server_name)}
        else:
            for server_name, server in self._servers.items():
                error_dict[server_name] = server.get('device').get_error()
        
        return error_dict
    
    async def add_server(self, **kwargs):
        """
        Adds a server to the Controller
        :return: None
        """
        if kwargs.get('name') in self._servers:
            return self._servers.get(kwargs.get('name')).get('device')
        self._logger.file_console.info('Adding Server {0} at {1}:{2}'.format(kwargs.get('name', 'unknown'),
                                                                             kwargs.get('ip_address', 'unknown'),
                                                                             kwargs.get('port', 'unknown')))
        
        new_server = Server(kwargs, log_level=getattr(self, 'server_log_level', 'INFO'),
                            log_disabled=getattr(self, 'server_log_disabled', False),
                            loop=self._loop)
        new_server.event_on_device_attach += self._event
        new_server.event_on_device_detach += self._event
        new_server.event_on_server_connect += self._event
        new_server.event_on_server_disconnect += self._event
        new_server.event_on_error += self._event
        self._servers[kwargs.get('name')] = {'device': new_server, 'connected': False}
        
        if kwargs.get('auto_connect', False):
            new_server.connect()
        return new_server
    
    async def add_device(self, **kwargs):
        """
        Adds or updates device info
        :param kwargs: keywords of device info
        :return:
        """
        self._logger.file_console.info(
                'Adding Device {0} serial {1} from {2} type {3}'.format(kwargs.get('name', 'unknown'),
                                                                        kwargs.get('serial', 0),
                                                                        kwargs.get('address',
                                                                                   'unknown'),
                                                                        kwargs.get('type',
                                                                                   'unknown')))
        
        device: Device = self._devices.pop(str(kwargs.get('serial')), {'device': None,
                                                                       'name': kwargs.get('name', 'unknown'),
                                                                       'connected': kwargs.get('connected', False),
                                                                       'serial': kwargs.get('serial'),
                                                                       'type': kwargs.get('type'), })
        if device.get('device', None) is None:
            new_device = Device(kwargs, log_level=getattr(self, 'device_log_level', 'INFO'),
                                log_disabled=getattr(self, 'device_log_disabled', False),
                                loop=self._loop)
            new_device.event_on_output_change += self._event
            new_device.event_on_cycle_update += self._event
            new_device.event_on_cycle_start += self._event
            new_device.event_on_cycle_end += self._event
            new_device.event_on_error += self._event
            new_device.event_on_set_start += self._event
            new_device.event_on_set_end += self._event
            device['device'] = new_device
        else:
            device.update(kwargs)
        
        self._devices[kwargs.get('serial')] = device
        self._devices_states.update({kwargs.get('serial'): {'connected': True}})
        return device
    
    def connect(self, *server_names: str):
        if server_names:
            s_names = set(server_names)
            for server_name in s_names:
                server_obj = self._servers.get(server_name, {'device': None})['device']  # type: Server
                if server_obj is not None:
                    server_obj.connect()
                else:
                    return False
        else:
            for s_name, server in self._servers.items():
                server_obj = self._servers.get(s_name, {'device': None})['device']  # type: Server
                server_obj.connect()
    
    def disconnect(self, *server_names: str):
        if server_names:
            s_names = set(server_names)
            for s_name in s_names:
                server_obj: Server = self._servers.get(s_name, {'device': None})['device']  # type: Server
                if server_obj is not None:
                    server_obj.disconnect()
                else:
                    return False
                ip_address = server_obj.data.get('ip_address')
                port = server_obj.data.get('port')
                # remove devices attached to that server
                remove_list = []
                for serial, device in self._devices.items():
                    device_obj = device.get('device')
                    if device_obj.address == ip_address and device_obj.port == port:
                        remove_list.append(serial)
                
                for serial in remove_list:
                    self._devices.pop(serial)
                # remove server from servers
                self._servers.pop(s_name)
        else:
            for s_name, server in self._servers.items():
                server_obj = self._servers.get(s_name, {'device': None})['device']  # type: Server
                server_obj.disconnect()
            self._devices.clear()
            self._servers.clear()
    
    def connection_status(self):
        return {server: self._servers[server].get('connected', 'unknown') for server in self._servers.keys()}
    
    def get_scheduled_tasks(self):
        return self._scheduler.get_running_tasks()
    
    async def run_device_command(self, command):
        data = command.get('data')
        device_serial = int(data.get('device'))
        device_data: dict = self._devices.get(device_serial)
        # if device_data is not None:
        device: Device = device_data.get('device')
        # if device is not None:
        try:
            return await device.run_command(command)
        except IOError as e:
            return False
    
    def get_available_devices(self, *server_names: str):
        available_devices = {}
        if server_names:
            s_names = set(server_names)
            for server_name in s_names:
                server_obj = self._servers.get(server_name, {'device': None})['device']  # type: Server
                if server_obj is not None:
                    available_devices[server_name] = server_obj.get_available_devices()
                else:
                    return False
        else:
            for server_name, server in self._servers.items():
                server_obj = self._servers.get(server_name, {'device': None})['device']  # type: Server
                available_devices[server_name] = server_obj.get_available_devices()
        
        return available_devices
    
    def get_device_states(self, device=None):
        if device is None:
            return self._devices_states
        else:
            return self._devices_states.get(device)
    
    def _event(self, e: ControllerEvent):
        self._logger.file_console.debug('Got {} event {}'.format(e.event, e.data))
        if isinstance(e, ServerEvent):
            if e.type == ServerEvent.SERVER_CONNECT:
                self._servers[e.data.get('name')].update({'connected': True})
            elif e.type == ServerEvent.SERVER_DISCONNECT:
                self._servers[e.data.get('name')].update({'connected': False})
            elif e.type == ServerEvent.DEVICE_ATTACH:
                if e.data['serial'] in self._devices:
                    # pretty_print(self._devices)
                    self._devices[e.data.get('serial')].update({'connected': True})
                    self._devices_states.update({e.data.get('serial'): {'connected': True}})
                else:
                    asyncio.run_coroutine_threadsafe(self.add_device(**e.data, connected=True), loop=self._loop)
            elif e.type == ServerEvent.DEVICE_DETACH:
                # pretty_print(self._devices)
                self._devices[int(e.data.get('serial'))].update({'connected': False})
                self._devices_states.update({e.data.get('serial'): {'connected': False}})
            elif e.type == ServerEvent.SERVER_ERROR:
                self._logger.file_console.error("Server error {0}".format(e.data))
            else:
                self._logger.file_console.error("Unknown Server Event Occurred")
                return
        elif isinstance(e, DeviceEvent):
            if e.type == DeviceEvent.CYCLE_START:
                self._logger.file_console.debug("Cycle started: {}".format(e.data))
            elif e.type == DeviceEvent.CYCLE_END:
                self._logger.file_console.debug("Cycle ended: {}".format(e.data))
            elif e.type == DeviceEvent.OUTPUT_CHANGE:
                self._logger.file_console.debug("Output Change Detected: {}".format(e.data))
                serial = e.data.get('serial')
                index = e.data.get('index')
                state = e.data.get('state')
                data = self._devices_states.get(serial)
                if data is None:
                    self._devices_states.update({serial: {index: state}})
                else:
                    data.update({index: state})
            
            elif e.type == DeviceEvent.CYCLE_UPDATE:
                self._logger.file_console.debug("Cycle Update: {}".format(e.data))
            elif e.type == DeviceEvent.DEVICE_ERROR:
                self._logger.file_console.error("Device error {0}".format(e.data))
            elif e.type in [DeviceEvent.SET_START, DeviceEvent.SET_END]:
                self._logger.file_console.debug("Set Update: {}".format(e.data))
            else:
                self._logger.file_console.error("Unknown Device Event Occurred")
                return
        else:
            self._logger.file_console.error("Unknown Event Occurred")
            return
        
        event = ControllerEvent(event=e.event, data=e.data, type=e.type)
        event.data.update({'event': e.event[9:]})
        events = getattr(self, e.event)
        events(event)
        
        # an all subscribe event
        events = getattr(self, 'event_on_any')
        events(event)


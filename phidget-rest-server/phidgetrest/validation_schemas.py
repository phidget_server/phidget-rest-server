from cerberus import Validator, DocumentError, SchemaError
from flask import abort
import time

validator = Validator()


class InvalidUsage(Exception):
    status_code = 400
    
    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload
    
    def to_dict(self):
        rv = dict(self.payload or ())
        if isinstance(self.message, dict):
            rv.update(self.message)
        else:
            rv['message'] = self.message
        return rv


def error_response(validation_error, message="Command Validation Fail", code=400):
    respose = {
        '_status': 'ERR',
        '_issues': validation_error,
        '_error': {
            'code': code,
            'message': message
        }
    }
    return respose


def _validate(data, schema):
    try:
        validator.schema = schema
        normalized_data = validator.normalized(data)
        if not validator.validate(normalized_data, schema):
            return error_response(validator.errors, 'Command Validation Error'), data
    except SchemaError as e:
        return error_response(str(e), 'Schema Error'), data
    except DocumentError as e:
        return error_response(str(e), 'Document Error'), data
    else:
        return False, normalized_data


def validate_command(data: dict):
    commands = data.get('commands')
    if not len(commands):
        error = error_response('No commands supplied', 'Command Error')
    else:
        # validate main command
        error, normalized_data = _validate(data, device_command_schema)
    
    if error:
        return error, data
    else:
        data.update(normalized_data)
    
    for command in commands:
        action = command.get('action', '')
        if 'set' in action:
            error, normalized_command = _validate(command, command_set_schema)
        elif action == 'cycle':
            error, normalized_command = _validate(command, command_cycle_schema)
        else:
            error = error_response('action is not valid or not supplied, received: {}'.format(action), 'Command Error')
        
        if error:
            # error.update({'bad_commands': command})
            return error, normalized_data
        else:
            command.update(normalized_command)
    return False, data


device_command_schema = {
    'action': {
        'type': 'string',
        'required': True,
        'allowed': ['device_command']
    },
    'auto_add_resource': {
        'type': 'boolean',
        'default': True
    },
    'commands': {
        'type': 'list',
        'schema': {
            'type': 'dict',
            'required': True,
        },
        'required': True,
    },
}
command_set_schema = {
    'action': {'type': 'string', 'allowed': ['set_closed', 'set_open', 'set_toggle'], 'required': True},
    'ports': {'type': 'list', 'required': True, 'schema': {'type': 'integer'}},
    'device': {'type': 'string', 'required': True},
    'start_time': {'type': ['integer', 'float']},
}

command_cycle_schema = {
    'action': {'type': 'string', 'allowed': ['cycle'], 'required': True},
    'ports': {'type': 'list', 'required': True},
    'device': {'type': 'string', 'required': True},
    'cycles': {'type': 'integer', 'required': True},
    'start_time': {'type': ['integer', 'float'], 'default': time.time()},
    'time_closed': {'type': ['list', 'integer', 'float'], 'schema': {'type': ['integer', 'float']}},
    'time_open': {'type': ['list', 'integer', 'float'], 'schema': {'type': ['integer', 'float']}},
    'state_start': {'type': 'string', 'allowed': ['set_open', 'set_closed'], 'default': 'set_closed'},
    'state_end': {'type': 'string', 'allowed': ['set_open', 'set_closed'], 'default': 'set_open'},
}

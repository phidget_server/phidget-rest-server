from pprint import PrettyPrinter

def pretty_print(this):
    p = PrettyPrinter(width=120, compact=True)
    p.pprint(this)

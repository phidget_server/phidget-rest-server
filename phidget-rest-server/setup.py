from setuptools import setup

setup(
        name='phidgetrest',
        version='0.1',
        packages=['phidgetrest'],
        install_requires= [
            'aiohttp==2.3.10',
            'asyncio==3.4.3',
            'baselogger==1.0.0',
            'Eve==0.8.dev0',
            'Eve-Swagger==0.0.9',
            'future==0.16.0',
            'Phidgets==2.1.9',
            'pymongo==3.6.1',
            'pytest==3.5.0',
            'pytest-asyncio==0.8.0',
            'pytest-mock==1.9.0',
            'sortedcontainers==1.5.9'
            ],
        url='https://gitlab.com/elrichindy/phidgetrest',
        license='MIT',
        author='elric.hindy',
        author_email='elrichindy@gmail.com',
        description='rest api for controlling phidgets devices',
        )
